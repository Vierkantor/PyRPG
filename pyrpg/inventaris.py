"""pyrpg.inventaris - Wat men zoal mee kan nemen."""

from zond import Betrekking, Zaak


class Vastgehouden(Betrekking):
    """Geeft aan dat een zaak door een wezen wordt vastgehouden."""

    grijper = Zaak
    gegrepen = Zaak


def voorhanden(zond, wezen):
    """Bepaal alle zaken die door dit wezen wordt vastgehouden."""
    return zond.zeef(Vastgehouden.grijper, wezen).waarden(Vastgehouden.gegrepen)
