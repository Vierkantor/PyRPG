"""prpg.gebouw - Huizen, werkplaatsen, hallen en vergelijkbare constructies."""

from zond import Betrekking, Zaak


class Thuis(Betrekking):
    """Een personage kan een thuis hebben, waar diegene slaapt enzo.

    Een gebouw kan meerdere bewoners hebben: familie, of vrienden, of huisgenoten.
    Net zo kan iemand bewoner zijn van meerdere gebouwen, bijvoorbeeld een voor de zomer en een voor de winter.
    """

    bewoner = Zaak
    gebouw = Zaak


class Ingang(Betrekking):
    """Gebouwen hebben vaak een ingang zoals een voordeur of achterdeur.

    Dit kan ook gebruikt worden voor iets abstracters zoals tenten of hekken.
    """

    omhulsel = Zaak
    omhuld = Zaak


class InGebouw(Betrekking):
    """Deze plek maakt deel uit van een gebouw."""

    plek = Zaak
    gebouw = Zaak


def gebouw_van(zond, plek):
    """Geef het gebouw waar deze plek bij hoort."""
    return zond.geef(InGebouw.plek, plek).gebouw
