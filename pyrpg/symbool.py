"""pyrpg.symbool - Stop namespaces in je dict!
"""


class Symbool:
    """Object met zijn id als key.

    Je hebt wel eens dat je volkomen ongerelateerde waarden met dezelfde term aan wilt duiden.
    Dit kan voor problemen zorgen als je die dingen in dezelfde dict wilt stoppen.
    Een mooie oplossing is door middel van symbolen:
    objecten die als dict-key hun id hebben.
    Dit garandeert dat twee aangemaakte symbolen nooit dezelfde index krijgen.

    Hier is een voorbeeld van code die misgaat:

    def a(dict):
        dict['kleur'] = 'rood'
        b(dict)
        return dict['kleur'] # geeft 'blauw'
    def b(dict):
        dict['kleur'] = 'blauw'

    Met een Symbool kun je dat voorkomen:

    def a(dict):
        kleur = Symbool('kleur')
        dict[kleur] = 'rood'
        b(dict)
        return dict[kleur] # geeft 'rood'
    def b(dict):
        kleur = Symbool('kleur')
        dict[kleur] = 'blauw'
    """

    def __init__(self, naam):
        self.naam = naam

    def __hash__(self):
        return id(self)
