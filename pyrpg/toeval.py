"""pyrpg.toeval - Genereer toevallige uitkomsten."""

import random


def d(aantal, vlakken=None):
    """Werp dobbelstenen met gegeven aantal vlakken en sommeer de uitkomst.

    Heeft twee aanroepsvormen:
    `d(2, 6)` werpt twee zesvlakkige dobbelstenen en telt uitkomsten op (voldoen dus aan 2 <= n <= 12),
    `d(10)` werpt een tienvlakkige dobbelsteen (en is dus een afkorting voor `d(1, 10)`)
    """

    # Je mag de eerste waarde weglaten, maar in Python moet je verplicht voor onverplicht laten komen.
    # Hier wordt dat opgevangen.
    if vlakken is None:
        vlakken = aantal
        aantal = 1

    uitkomst = 0
    for _ in range(0, aantal):
        # Merk op dat randint inclusief eindpunten werkt!
        uitkomst += random.randint(1, vlakken)
    return uitkomst


def kies_willekeurig(lijstje):
    """Gegeven een lijstje waarden, kies een willekeurige."""
    return random.choice(lijstje)


def pak_d_willekeurig(lijstje, aantal, vlakken=None):
    """Werp een dobbelsteen voor het aantal en kies zoveel elementen uit het lijstje zonder terugleggen.

    :param lijstje: iets enumereerbaars, bijvoorbeeld een `list` of `set`
    """
    uitkomst = d(aantal, vlakken)
    return random.sample(list(lijstje), uitkomst)
