"""pyrpg.toevalsmagie - Een grote lijst toevallige magische gebeurtenissen.

Bronnen hiervoor zijn o.a. de Net Librams of Random Magical Effects (versies 1.20 en 2.0),
aangeduid met NLRME1 en NLRME2 respectievelijk.

Een gevolg wordt gemodelleerd als een aanroepbaar object (doorgaans een functie),
die een onderwerp (degene die de magie gebruikt/veroorzaakt),
en een voorwerp (het doel(wit) van de magie, of None indien irrelevant) neemt.
Mooie manieren om gevolgen te definieren:

    @gevolg
    def iets_gebeurt(onderwerp, voorwerp):
        pass

    from pyrpg.reguliere_magie import iets_anders_gebeurt
    gevolg(iets_anders_gebeurt)

Voor testbaarheid kun je dingen teruggeven in de functie, maar hier wordt niets mee gedaan.

Een gevolg kan een Exception van type ProbeerOpnieuw gooien als de situatie niet toepasbaar is.
Denk bijvoorbeeld aan een gevolg als `je kat wordt opeens een hond',
maar de speler heeft geen kat.
Indien dit gegooid wordt, mag het effect niets hebben gedaan.
(Dit zodat we opnieuw iets willekeurigs kunnen uitkiezen.)
"""
from pyrpg import Plek
from pyrpg.effect import Effect, EffectVerdwijnt
from pyrpg.gebouw import Ingang, Thuis
from pyrpg.gedrag import Volgen, geef_doelstelling, nuttig
from pyrpg.lichaamsbouw.mens import Vinger
from pyrpg.plek import Begroeiing, Begroeiingssoort, Omgeving, Richting, Verbinding
from pyrpg.poel import Poel
from pyrpg.stof import BestaatUit, Steen, maak_van_olijfolie
from pyrpg.taal import opsomming
from pyrpg.tijd import uit_dagen
from pyrpg.toeval import d, kies_willekeurig, pak_d_willekeurig
from pyrpg.wezens.eend import maak_eendje
from zond import NietUniekError

# De daadwerkelijke lijst toevalsmagische gevolgen.
_gevolgen = []


def geef_toevallig_gevolg():
    """Kies een toevallig gevolg uit de lijst."""
    return kies_willekeurig(_gevolgen)


class ProbeerOpnieuw(Exception):
    """Wordt door een gevolg gegooid als de situatie verhindert dat het effect kan worden toegepast."""


def pas_toevallig_gevolg_toe(zond, afteller, onderwerp, voorwerp=None):
    """Kies een toevallig gevolg uit de lijst en pas dat toe.

    We proberen toevallige gevolgen net zolang tot we eentje vinden die toepasbaar is.
    (In principe zou dit redelijk snel moeten gebeuren.)

    :param zond: Een Zond-object voor de wereld van het onderwerp en het voorwerp.
    :type zond: Zond
    :param onderwerp: Degene die het gevolg heeft veroorzaakt, bijvoorbeeld door een spelfout in een toverspreuk.
    :type onderwerp: Zaak
    :param voorwerp: Het originele doel van de magie, bijvoorbeeld het doel van de toverspreuk die misliep.
    :type voorwerp: None|Zaak
    """

    # TEDOEN: als het te lang duurt voordat we iets vinden, geven we een foutmelding of gebeurt er dan niets?
    while True:
        mogelijk_gevolg = geef_toevallig_gevolg()
        try:
            return mogelijk_gevolg(zond, afteller, onderwerp, voorwerp)
        except ProbeerOpnieuw:
            continue


def gevolg(functie):
    """Decorator voor een gevolg."""
    _gevolgen.append(functie)
    return functie


@gevolg
def onderwerps_vingers_verstenen(zond, _afteller, onderwerp, _voorwerp):
    """NLRME2-0001: 1d10 van de vingers van het onderwerp verstenen.

    Geeft de versteende vingers terug.
    """
    steensoorten = list(zond.opsomming(Steen))
    if not steensoorten:
        raise ProbeerOpnieuw()
    alle_vingers = list(zond.zoek(Vinger.wezen, onderwerp))
    if not alle_vingers:
        raise ProbeerOpnieuw()

    versteende_vingers = pak_d_willekeurig(alle_vingers, 10)
    steen = kies_willekeurig(steensoorten)

    for vinger in versteende_vingers:
        zond.betrek(BestaatUit, voorwerp=vinger.deel, stof=steen.stof)
    return versteende_vingers


@gevolg
def zwerm_bijen_rond_onderwerp(zond, afteller, onderwerp, _voorwerp):
    """NLRME2-0002: een zwerm van 1d100 bijen vliegt een paar weken ongevaarlijk om het hoofd van het onderwerp.

    Geeft het veroorzaakte effect terug.
    """
    aantal_bijen = d(1, 100)
    aantal_dagen = d(7, 6)

    effect = zond.betrek(
        Effect,
        onderwerp=onderwerp,
        beschrijving=f"een zwerm van {aantal_bijen} bijen vliegt om hun hoofd.",
    )
    afteller.stel_gebeurtenis_in(uit_dagen(aantal_dagen), EffectVerdwijnt(zond, effect))

    return effect


@gevolg
def lichtende_puntjes_rond_onderwerp(zond, afteller, onderwerp, _voorwerp):
    """NLRME2-0003: 1d100 lichtende puntjes dansen rond het hoofd van het onderwerp tot zonsopkomst.

    Geeft het veroorzaakte effect terug.
    """
    aantal_puntjes = d(1, 100)
    effect = zond.betrek(
        Effect,
        onderwerp=onderwerp,
        beschrijving=f"er dansen {aantal_puntjes} lichtende puntjes om hun hoofd.",
    )
    afteller.stel_gebeurtenis_in(
        afteller.bij_zonsopkomst(), EffectVerdwijnt(zond, effect)
    )

    return effect


@gevolg
def onderwerps_lichaam_verijzerd(zond, afteller, onderwerp, _voorwerp):
    """NLRME2-0004: 1d100 procent van het lichaam van het onderwerp wordt ijzer.

    Geeft het veroorzaakte effect terug.
    """
    procenten = d(1, 100)
    tijd = d(1, 100)
    effect = zond.betrek(
        Effect,
        onderwerp=onderwerp,
        beschrijving=f"{procenten} deel van hun lichaam is van ijzer.",
    )
    afteller.stel_gebeurtenis_in(tijd, EffectVerdwijnt(zond, effect))

    return effect


@gevolg
def eendjes_volgen_onderwerp(zond, _afteller, onderwerp, _voorwerp):
    """NLRME2-0005: 1d12 eendjes volgen het onderwerp als hun ouder.

    Geeft een lijst met de eendjes terug.
    """

    try:
        plek = zond.geef(Plek.wat, onderwerp).waar
    except NietUniekError as e:
        raise ProbeerOpnieuw() from e

    aantal = d(1, 12)
    eendjes = []
    for _ in range(0, aantal):
        eendje = maak_eendje(zond, plek)
        geef_doelstelling(zond, eendje, Volgen, onderwerp, prioriteit=nuttig)
        eendjes.append(eendje)

    return eendjes


@gevolg
def onderwerps_vingers_verhuizen(zond, _afteller, onderwerp, _voorwerp):
    """NLRME2-0006: 1d4 van de vingers van het onderwerp verhuizen van de linker- naar de rechterhand.

    Geeft het veroorzaakte effect terug.
    """
    aantal = d(1, 4)
    effect = zond.betrek(
        Effect,
        onderwerp=onderwerp,
        beschrijving=f"hun linkerhand heeft {5-aantal} vingers en hun rechterhand {5+aantal}.",
    )
    return effect


@gevolg
def onderwerps_ledematen_als_staal(zond, _afteller, onderwerp, _voorwerp):
    """NRLME2-0007: 1d4 van de ledematen van het onderwerp zijn sterk als staal.

    Geeft het veroorzaakte effect terug.
    """
    opties = {"linkerarm", "rechterarm", "linkerbeen", "rechterbeen"}
    ledematen = pak_d_willekeurig(opties, 4)
    effect = zond.betrek(
        Effect,
        onderwerp=onderwerp,
        beschrijving=f"hun {opsomming(ledematen)} is zo sterk als staal.",
    )
    return effect


@gevolg
def onderwerps_ledematen_geschubd(zond, _afteller, onderwerp, _voorwerp):
    """NRLME2-0008: 1d4 van de ledematen van het onderwerp zijn bedekt met vissenschubben.

    Geeft het veroorzaakte effect terug.
    """
    opties = {"linkerarm", "rechterarm", "linkerbeen", "rechterbeen"}
    ledematen = pak_d_willekeurig(opties, 4)
    effect = zond.betrek(
        Effect,
        onderwerp=onderwerp,
        beschrijving=f"hun {opsomming(ledematen)} is bedekt met vissenschubben.",
    )
    return effect


@gevolg
def onderwerps_ledematen_onzichtbaar(zond, _afteller, onderwerp, _voorwerp):
    """NRLME2-0009: 1d4 van de ledematen van het onderwerp zijn onzichtbaar.

    Geeft het veroorzaakte effect terug.
    """
    opties = {"linkerarm", "rechterarm", "linkerbeen", "rechterbeen"}
    ledematen = pak_d_willekeurig(opties, 4)
    effect = zond.betrek(
        Effect,
        onderwerp=onderwerp,
        beschrijving=f"hun {opsomming(ledematen)} is onzichtbaar.",
    )
    return effect


@gevolg
def onderwerps_lichaamsopeningen_dicht(zond, _afteller, onderwerp, _voorwerp):
    """NRLME2-0010: 1d6 van de lichaamsopeningen van het onderwerp gaan dicht.

    Geeft het veroorzaakte effect terug.
    """
    opties = {
        "linkeroog",
        "rechteroog",
        "linkeroor",
        "rechteroor",
        "linkerneusgat",
        "rechterneusgat",
        "mond",
    }
    lichaamsopeningen = pak_d_willekeurig(opties, 6)
    effect = zond.betrek(
        Effect,
        onderwerp=onderwerp,
        beschrijving=f"hun {opsomming(lichaamsopeningen)} zit helemaal dicht.",
    )
    return effect


@gevolg
def onderwerp_krijgt_bulten(zond, _afteller, onderwerp, _voorwerp):
    """NRLME2-0011: het lichaam van het onderwerp wordt bedekt in bulten ter grootte van een walnoot.

    Geeft het veroorzaakte effect terug.
    """
    aantal = d(3, 10)
    effect = zond.betrek(
        Effect,
        onderwerp=onderwerp,
        beschrijving=f"hun lichaam is bedekt in {aantal} bulten ter grootte van een walnoot.",
    )
    return effect


@gevolg
def onderwerp_krijgt_olijfolie_uit_oren(zond, _afteller, onderwerp, _voorwerp):
    """NRLME2-0012: uit de oren van het onderwerp lopen 3d10 liters olijfolie.

    Geeft het veroorzaakte effect terug.
    """

    try:
        plek = zond.geef(Plek.wat, onderwerp).waar
    except NietUniekError as e:
        raise ProbeerOpnieuw from e

    hoeveelheid = d(3, 10)
    effect = zond.betrek(
        Effect,
        onderwerp=onderwerp,
        beschrijving=f"uit hun oren heeft zojuist {hoeveelheid} liter olijfolie gestroomd.",
    )

    poel = zond.zaak()
    maak_van_olijfolie(zond, poel)
    zond.betrek(Poel, plek=plek, poel=poel)

    return effect


@gevolg
def onderwerp_krijgt_ogen(zond, _afteller, onderwerp, _voorwerp):
    """NRLME2-0013: het gezicht en hoofd van het onderwerp wordt bedekt met 4d6 nutteloze ogen.

    Geeft het veroorzaakte effect terug.
    """
    aantal = d(4, 6)
    effect = zond.betrek(
        Effect,
        onderwerp=onderwerp,
        beschrijving=f"op hun hoofd zitten {aantal} extra ogen.",
    )
    return effect


@gevolg
def begroeiing_kaal_rond_huis_onderwerp(zond, _afteller, onderwerp, _voorwerp):
    """NRLME2-0014: in de omgeving van het huis van het onderwerp wordt alle begroeiing kaalgeslagen."""

    # Kies willekeurig een omgeving van een thuis uit.
    omgevingen = list(
        zond.zeef(Thuis.bewoner, onderwerp)
        .bind(Thuis.gebouw, Omgeving.binnenin)
        .waarden(Omgeving.buitenom)
    )
    if not omgevingen:
        raise ProbeerOpnieuw()
    omgeving = kies_willekeurig(omgevingen)

    # Stel de begroeiing in op kaal door update of insert.
    # TEDOEN: bouw in Zond een update-of-insert in
    try:
        begroeiing = zond.geef(Begroeiing.plek, omgeving)
        begroeiing.begroeiing = Begroeiingssoort.kaal
    except NietUniekError:
        zond.betrek(Begroeiing, plek=omgeving, begroeiing=Begroeiingssoort.kaal)


@gevolg
def omgeving_onderwerp_zinkt_weg(zond, _afteller, onderwerp, _voorwerp):
    """NRLME2-0015: de directe omgeving rond het onderwerp zinkt weg in de aarde.

    :return: Geeft de nieuwe plaats terug.
    """

    # Maak een put aan die omhoog aan de omgeving is verbonden.
    try:
        plek = zond.geef(Plek.wat, onderwerp)
    except NietUniekError as e:
        raise ProbeerOpnieuw() from e
    put = zond.zaak()
    Verbinding.betrek_symmetrisch(zond, put, plek.waar, Richting.op)
    # En stop het onderwerp erin.
    zond.onttrek(plek)
    zond.betrek(Plek, wat=onderwerp, waar=put)

    return put


@gevolg
def kelder_in_huis_onderwerp(zond, _afteller, onderwerp, _voorwerp):
    """NRLME2-0016: het huis van de onderwerp krijgt een kelder.

    :return: Geeft de nieuwe plaats terug.
    """

    # Kies een kamer aan de ingang van het huis.
    kamers = list(
        zond.zeef(Thuis.bewoner, onderwerp)
        .bind(Thuis.gebouw, Ingang.omhulsel)
        .waarden(Ingang.omhuld)
    )
    if not kamers:
        raise ProbeerOpnieuw()

    # Ga zoveel mogelijk naar beneden.
    # TODO: geef op na zoveel stappen naar beneden proberen te gaan?
    minima = set()
    while kamers:
        kamer = kamers.pop()
        eronder = list(
            zond.zeef(Verbinding.van, kamer)
            .zeef(Verbinding.richting, Richting.neer)
            .waarden(Verbinding.naar)
        )
        if not eronder:
            minima.add(kamer)
        else:
            kamers.extend(eronder)
    if not minima:
        raise ProbeerOpnieuw()

    # En begin daar met een kelder graven.
    kamer = kies_willekeurig(list(minima))
    kelder = zond.zaak()
    Verbinding.betrek_symmetrisch(zond, kelder, kamer, Richting.op)

    return kelder
