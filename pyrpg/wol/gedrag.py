"""pyrpg.wol.gedrag - Hoe men aan wol komt."""

from pyrpg.gedrag import VoorwerpHier, VoorwerpVoorhanden, klaar_over, voorwerpen_op_plek_met
from pyrpg.eenheid import Grootheid, kilo
from pyrpg.plek import waar_is
from pyrpg.productie import maak_produceerbaar_voorwerp_voorhanden, produceer_goed
from pyrpg.tijd import uit_minuten
from pyrpg.toeval import kies_willekeurig
from pyrpg.wol.goed import Scheerwol, GereinigdeWol, GekaardeWol, Wolgaren, kaarden, spinnen, wassen, wezen_heeft_wol
from zond import BestaatNietError

class ScheerwolVoorhanden(VoorwerpVoorhanden):
    def __init__(self):
        def is_goed(zond, wat):
            return bool(list(zond.zeef(Scheerwol.goed, wat)))
        super().__init__(is_goed)

    def oplossingen(self, zond, wezen):
        resultaat = super().oplossingen(zond, wezen)
        def scheren(zond, afteller, wezen):
            # Bron: https://www.dependablepickup.com/how-long-does-it-take-to-shear-a-sheep/
            produceer_goed(zond, wezen, Scheerwol, Grootheid(5, kilo), [kies_willekeurig(list(voorwerpen_op_plek_met(zond, waar_is(zond, wezen), wezen_heeft_wol)))])
            klaar_over(zond, afteller, wezen, uit_minuten(30))
            return True
        resultaat.append((scheren, [VoorwerpHier(wezen_heeft_wol)]))
        return resultaat

Scheerwol.gedrag_voorhanden = ScheerwolVoorhanden
GereinigdeWol.gedrag_voorhanden = GereinigdeWolVoorhanden = maak_produceerbaar_voorwerp_voorhanden(GereinigdeWol, [wassen])
GekaardeWol.gedrag_voorhanden = GekaardeWolVoorhanden = maak_produceerbaar_voorwerp_voorhanden(GekaardeWol, [kaarden])
Wolgaren.gedrag_voorhanden = WolgarenVoorhanden = maak_produceerbaar_voorwerp_voorhanden(Wolgaren, [spinnen])
