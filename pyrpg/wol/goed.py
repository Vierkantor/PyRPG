"""pyrpg.wol - Wol en het productieproces eromheen."""


from pyrpg.eenheid import Grootheid, gram, kilo, minuut, uur
from pyrpg.goed import Bulkgoed
from pyrpg.kruising import KruisingSoort
from pyrpg.productie import maak_evenredig_productieproces
from pyrpg.toeval import kies_willekeurig
from pyrpg.wezen import Wezen
from zond import BestaatNietError, Betrekking, Zaak


def wezen_heeft_wol(zond, wezen):
    """Heeft dit wezen wol dat afgeschoren kan worden?"""
    try:
        kruisinginfo = zond.geef(Wezen.wezen, wezen).kruisinginfo
    except BestaatNietError:
        return False
    return kruisinginfo.soort in {KruisingSoort.schaap}


class Wolsoort(Betrekking):
    """Geef aan dat deze Zaak een soort wol is, behorend bij een soort wezens.

    Merk op dat we in principe niet verbieden dat de wol afkomstig is
    van een soort die normaliter niet wol draagt, zoals eenden.
    """

    soort = KruisingSoort
    wol = Zaak


def geef_wolsoort_van_soort(zond, soort):
    alle_soorten = zond.alles(Wolsoort).alleen(Wolsoort)
    wolsoorten = [wolsoort for wolsoort in alle_soorten if wolsoort.soort == soort]
    if wolsoorten:
        wol = kies_willekeurig(wolsoorten).wol
    else:
        wol = zond.zaak()
        zond.betrek(Wolsoort, soort=soort, wol=wol)
    return wol


def geef_wolsoort_van_wezens(zond, invoer):
    # TEDOEN: we gaan ervan uit dat de invoer bestaat uit alleen een schaap
    assert len(invoer) == 1
    schaap = invoer[0]
    return geef_wolsoort_van_soort(zond, zond.geef(Wezen.wezen, schaap).kruisinginfo.soort)


def geef_wolsoort_van_goederen(zond, invoer, kolom):
    """Zoek door de invoer op iets van wol, en geef de bijbehorende `Wolsoort'.

    :param invoer: een lijst paren (zaak, hoeveelheid).
    :param kolom: de `goed'-kolom van een `Bulkgoed'-betrekking.
    """
    # TEDOEN: zou netter zijn als we niet per kolom zouden hoeven te werken:
    # ontkoppel dat het van wol is en dat het een bepaald soort bulkgoed is?
    for wol, hoeveelheid in invoer:
        print(list(zond.ontkever_zaak(wol)))
        try:
            return zond.geef(kolom, wol).wolsoort
        except BestaatNietError:
            continue
    raise BestaatNietError(kolom, invoer, 0)

# Verschillende onderdelen van het wolproductieproces.
# Bronnen:
# http://textiel-handwerken.blogspot.com/search/label/wol
# https://de-gulle-aarde.blogspot.com/2012/01/spinnen-met-schapenwol.html
# https://www.newlifeonahomestead.com/how-to-clean-and-dry-raw-wool/
# http://www.sheep101.info/201/woolmarketing.html
class Scheerwol(Bulkgoed):
    """Geef aan dat deze Zaak vers geschoren wol is.

    Een doorsnee schaap heeft in de orde van 3 kg wol.
    Nadat de wol geschoren is, zit er nog veel viezigheid in,
    en bevat het veel wolvet.
    Dit moet er eerst uitgewassen worden.
    """

    wolsoort = Zaak

    @classmethod
    def produceer(cls, zond, hoeveelheid, invoer):
        wol = geef_wolsoort_van_wezens(zond, invoer)
        zaak = zond.zaak()
        zond.betrek(cls, goed=zaak, wolsoort=wol, massa=hoeveelheid)
        return zaak


class Wolvet(Bulkgoed):
    """Geef aan dat deze Zaak wolvet (of lanoline) is."""

    wolsoort = Zaak

    @classmethod
    def produceer(cls, zond, hoeveelheid, invoer):
        wol = geef_wolsoort_van_goederen(zond, invoer, Scheerwol.goed)
        zaak = zond.zaak()
        zond.betrek(cls, goed=zaak, wolsoort=wol, massa=hoeveelheid)
        return zaak


class GereinigdeWol(Bulkgoed):
    """Geef aan dat deze Zaak een hoeveelheid gereinigde wol is.

    Het bestaat nu nog uit een hoeveelheid ongerangschikte vezels.
    Door het kaarden en spinnen worden hier draden van gemaakt,
    waarmee verdere bewerkingen mogelijk worden.
    """

    wolsoort = Zaak

    @classmethod
    def produceer(cls, zond, hoeveelheid, invoer):
        wol = geef_wolsoort_van_goederen(zond, invoer, Scheerwol.goed)
        zaak = zond.zaak()
        zond.betrek(cls, goed=zaak, wolsoort=wol, massa=hoeveelheid)
        return zaak


wassen = maak_evenredig_productieproces(
    tijd=Grootheid(1, uur),
    invoeren=[(Scheerwol, Grootheid(1, kilo)),],
    uitvoeren=[(Wolvet, Grootheid(0.1, kilo)), (GereinigdeWol, Grootheid(0.75, kilo)),],
)


class GekaardeWol(Bulkgoed):
    """Geef aan dat deze Zaak een hoeveelheid gekaarde wol is.

    In gekaarde wol staan de vezels dezelfde kant op,
    zodat er bij het spinnen gemakkelijk een draad gevormd kan worden.
    """

    wolsoort = Zaak

    @classmethod
    def produceer(cls, zond, hoeveelheid, invoer):
        wol = geef_wolsoort_van_goederen(zond, invoer, GereinigdeWol.goed)
        zaak = zond.zaak()
        zond.betrek(cls, goed=zaak, wolsoort=wol, massa=hoeveelheid)
        return zaak


# TEDOEN: deze waarden zijn redelijk uit de lucht gegrepen
kaarden = maak_evenredig_productieproces(
    tijd=Grootheid(30, minuut),
    invoeren=[(GereinigdeWol, Grootheid(100, gram)),],
    uitvoeren=[(GekaardeWol, Grootheid(100, gram)),],
)


class Wolgaren(Bulkgoed):
    """Geef aan dat deze Zaak een hoeveelheid garen van wol is.

    Wolgaren is het eindproduct van het spinnen van wol,
    en kan bijvoorbeeld getwijnd en geslagen worden tot touwen,
    of gebreid of geweven worden tot stof.
    """

    wolsoort = Zaak

    @classmethod
    def produceer(cls, zond, hoeveelheid, invoer):
        wol = geef_wolsoort_van_goederen(zond, invoer, GekaardeWol.goed)
        zaak = zond.zaak()
        zond.betrek(cls, goed=zaak, wolsoort=wol, massa=hoeveelheid)
        return zaak


# TEDOEN: deze waarden zijn redelijk uit de lucht gegrepen
spinnen = maak_evenredig_productieproces(
    tijd=Grootheid(30, minuut),
    invoeren=[(GekaardeWol, Grootheid(100, gram)),],
    uitvoeren=[(Wolgaren, Grootheid(100, gram)),],
)
