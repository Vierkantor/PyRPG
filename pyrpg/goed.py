"""pyrpg.goed - Goederen, in allerlei soorten en maten."""


from pyrpg.eenheid import Grootheid
from zond import Betrekking, Zaak


class Bulkgoed(Betrekking):
    """Geef aan dat deze Zaak een hoeveelheid is van fungibele eenheden."""

    goed = Zaak
    massa = Grootheid
