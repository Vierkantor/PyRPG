"""pyrpg.poel - Definieert dat er een substantie op de grond kan liggen."""

from zond import Betrekking, Zaak


class Poel(Betrekking):
    """Een poel is een vloeibare substantie die ergens op de grond ligt."""

    plek = Zaak
    poel = Zaak
