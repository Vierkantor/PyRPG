"""pyrpg.voedsel - Definieert verscheidene soorten eten."""

from pyrpg.inventaris import voorhanden
from pyrpg.plek import Plek
from zond import Betrekking, Zaak


class Voedsel(Betrekking):
    """Geeft aan dat het voorwerp eetbaar is."""

    voorwerp = Zaak


def is_voedsel(zond, _wezen, voorwerp):
    """Is dit voedsel dat het wezen kan eten?"""
    # TODO: houd rekening met het wezen
    return bool(zond.zeef(Voedsel.voorwerp, voorwerp))


def eten_voorhanden(zond, wezen):
    """Bepaal alle voorwerpen die dit wezen mee heeft en kan eten."""
    return zond.zeef_zaken(Voedsel.voorwerp, voorhanden(zond, wezen)).waarden(
        Voedsel.voorwerp
    )


def eetbare_voorwerpen_op_plek(zond, _wezen, plek):
    """Bepaal alle voorwerpen op de plek die dit wezen kan eten."""
    # TODO: houd rekening met het wezen
    return (
        zond.zeef(Plek.waar, plek)
        .bind(Plek.wat, Voedsel.voorwerp)
        .waarden(Voedsel.voorwerp)
    )
