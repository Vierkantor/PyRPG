"""pyrpg.opslaan - Ondersteuning voor het opslaan en laden van de toestand."""

from pathlib import Path

from camel import Camel, CamelRegistry

from zond import zond_naar_yaml

# Bevat de opsla- en inlaaddefinities voor alle betrekkingen.
# Als je een relevante betrekking definieert, moet je die hierin registreren.
pyrpg_naar_yaml = CamelRegistry()


def sla_op(zond):
    """Gebruik `pyrpg_naar_yaml' om de wereld te serialiseren."""
    camel = Camel([zond_naar_yaml, pyrpg_naar_yaml])
    return camel.dump(zond)


def laad_in(zond):
    """Gebruik `pyrpg_naar_yaml' om de wereld te deserialiseren."""
    camel = Camel([zond_naar_yaml, pyrpg_naar_yaml])
    return camel.load(zond)


# Waar de opgeslagen werelden zich normaliter bevinden.
opslagmap = Path("./werelden")
