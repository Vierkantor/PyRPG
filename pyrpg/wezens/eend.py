"""pyrpg.wezens.eend - Eenden als voorgedefinieerde wezens."""
from pyrpg import Plek
from pyrpg.kruising import KruisingInfo, KruisingSoort
from pyrpg.wezen import Wezen


def maak_eend(zond, plek, titel="eend"):
    """Maak een nieuw wezen dat een (volwassen) eend is.

    :param zond: Het Zond-object waar dit wezen opgeslagen wordt.
    :type zond: Zond
    :param plek: De plek waar het eendje zich bevindt.
    :type plek: Zaak
    :param titel: Een titel voor deze eend. Mag iets zijn tussen een omschrijving en een specifieke naam.
    :type titel: str

    :rtype: Zaak
    """
    kruisinginfo = KruisingInfo(KruisingSoort.eend, {})
    wezen = zond.zaak()
    zond.betrek(Wezen, wezen=wezen, titel=titel, kruisinginfo=kruisinginfo)
    zond.betrek(Plek, wat=wezen, waar=plek)
    return wezen


def maak_eendje(zond, plek, titel="eendje"):
    """Maak een nieuw wezen dat een jong eendje is.

    :param zond: Het Zond-object waar dit wezen opgeslagen wordt.
    :type zond: Zond
    :param plek: De plek waar het eendje zich bevindt.
    :type plek: Zaak
    :param titel: Een titel voor deze eend. Mag iets zijn tussen een omschrijving en een specifieke naam.
    :type titel: str

    :rtype: Zaak
    """
    kruisinginfo = KruisingInfo(KruisingSoort.eend, {})
    wezen = zond.zaak()
    zond.betrek(Wezen, wezen=wezen, titel=titel, kruisinginfo=kruisinginfo)
    zond.betrek(Plek, wat=wezen, waar=plek)
    return wezen
