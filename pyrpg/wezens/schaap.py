"""pyrpg.wezens.schaap - Schapen als voorgedefinieerd wezen."""

from pyrpg.kruising import BoolseEigenschap, KruisingInfo, KruisingSoort
from pyrpg.symbool import Symbool
from pyrpg.wezen import Wezen

# Een nuttige bron voor de kleurgenetica van schapen: https://www.shaltzfarm.com/shcolor.html
wit = Symbool("wit")  # Awt versus Aa
zwart_ipv_bruin = Symbool("zwart_ipv_bruin")  # BB versus Bb
geen_vlekken = Symbool("geen_vlekken")  # SS versus Ss


def maak_schaap(zond, titel="Schaap", eigenschappen=None):
    """Maak een nieuw wezen dat een schaap is.

    Schapen zijn tamme grazers die bedekt zijn met wol.

    :param zond: De wereld voor dit wezen.
    :type zond: Zond
    :param titel: Een titel voor dit schaap. Mag iets zijn tussen een omschrijving en een specifieke naam.
    :type titel: str
    :param eigenschappen: Een dict met eigenschappen. Ontbrekende eigenschappen worden per toeval gegenereerd.
    :type eigenschappen: dict

    :rtype: Zaak
    """

    if not eigenschappen:
        eigenschappen = {}

    kruisinginfo = KruisingInfo(KruisingSoort.schaap, eigenschappen)
    kruisinginfo.bij_ontbreken(wit, BoolseEigenschap.geef_willekeurig)
    kruisinginfo.bij_ontbreken(zwart_ipv_bruin, BoolseEigenschap.geef_willekeurig)
    kruisinginfo.bij_ontbreken(geen_vlekken, BoolseEigenschap.geef_willekeurig)

    wezen = zond.zaak()
    zond.betrek(Wezen, wezen=wezen, titel=titel, kruisinginfo=kruisinginfo)
    return wezen


def is_wit(schaap):
    """Is dit schaap helemaal wit?"""
    return schaap.kruisinginfo.eigenschappen[wit].uitdrukking()


def is_zwart_ipv_bruin(schaap):
    """Heeft dit schaap geen bruine kleuring maar zwarte kleuring?"""
    return schaap.kruisinginfo.eigenschappen[zwart_ipv_bruin].uitdrukking()


def heeft_geen_vlekken(schaap):
    """Heeft dit schaap geen vlekken?

    Je kan niet altijd de vlekken zien, zelfs als de uitkomst False is:
    als het schaap wit is, zie je geen verschil met de witte ondergrond.
    Gebruik heeft_zichtbare_vlekken voor het antwoord op die vraag.
    """
    return schaap.kruisinginfo.eigenschappen[geen_vlekken].uitdrukking()


def heeft_zichtbare_vlekken(schaap):
    """Kan je de vlekken van dit schaap zien?

    Niet alleen moet het schaap aanleg tot vlekken hebben, maar de vlekken moeten ook nog eens niet-wit zijn.
    """
    return not (is_wit(schaap) or heeft_geen_vlekken(schaap))


def geef_vachtkleur(schaap):
    """Geef de kleur van de (tekening van de) vacht.

    De kleur kan zijn: 'bruin', 'wit', 'zwart'.
    (Als het schaap vlekken heeft, is dit de kleur van de vlekken. De ondergrond is altijd wit.)
    """
    if is_wit(schaap):
        return "wit"
    if is_zwart_ipv_bruin(schaap):
        return "zwart"
    return "bruin"


def beschrijf_vacht(schaap):
    """Geef een menselijk leesbare string die de vacht van dit schaap beschrijft."""

    vachtkleur = geef_vachtkleur(schaap)

    if heeft_zichtbare_vlekken(schaap):
        vlekomschrijving = "De vacht is wit met {}e vlekken."
    else:
        vlekomschrijving = "De vacht is helemaal {}."

    return vlekomschrijving.format(vachtkleur)
