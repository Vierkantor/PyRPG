"""pyrpg.productie - Voor het definiëren van productieprocessen."""


from pyrpg.eenheid import Grootheid
from pyrpg.gedrag import VoorwerpVoorhanden, klaar_over, voorwerpen_voorhanden_met
from pyrpg.inventaris import Vastgehouden
from pyrpg.toeval import kies_willekeurig
from zond import BestaatNietError, Betrekking, Zaak


class EvenredigProductieproces:
    """Representeert een productieproces.

    Hier wordt een zekere hoeveelheid bulkgoederen omgezet in andere bulkgoederen.
    De opbrengst en productietijd zijn recht evenredig met de invoer.
    """

    def __init__(self, tijd, invoeren, uitvoeren):
        self.tijd = tijd
        self.invoeren = invoeren
        self.uitvoeren = uitvoeren


def maak_evenredig_productieproces(tijd, invoeren, uitvoeren):
    """Maak een nieuw evenredig productieproces.

    De invoeren en uitvoeren zijn lijsten van paren (goed, hoeveelheid).

    :rtype: EvenredigProductieproces
    """

    return EvenredigProductieproces(tijd, invoeren, uitvoeren)


def gedrag_voorhanden(goed):
    """Geef een Gedrag dat ervoor zou moeten zorgen dat een gegeven goed voorhanden is."""
    try:
        return goed.gedrag_voorhanden()
    except AttributeError:
        return VoorwerpVoorhanden(lambda zond, wat: bool(list(zond.zeef(goed.goed, wat))))


def produceer_goed(zond, wezen, goed, hoeveelheid, invoerzaken):
    print(f"Produceren: {hoeveelheid} {goed}")
    zaak = goed.produceer(zond, hoeveelheid, invoerzaken)
    zond.betrek(Vastgehouden, grijper=wezen, gegrepen=zaak)
    assert list(genoeg_goed_voorhanden(zond, wezen, goed, hoeveelheid))
    return zaak


def hoeveelheid_voorhanden(zond, wezen, goed):
    """Geef de hoeveelheid van het goed dat voorhanden is."""
    def is_goed(zond, wat):
        try:
            zond.geef(goed.goed, wat)
            return True
        except BestaatNietError:
            return False

    voorhanden = voorwerpen_voorhanden_met(zond, wezen, is_goed)
    return max(zond.geef(goed.goed, wat).massa for wat in voorhanden)


def genoeg_goed_voorhanden(zond, wezen, goed, hoeveelheid):
    """Itereer over alle goederen voorhanden van het juiste type en minstens de juiste hoeveelheid."""
    def is_goed_genoeg(zond, wat):
        try:
            betrekking = zond.geef(goed.goed, wat)
        except BestaatNietError:
            return False
        return betrekking.massa >= hoeveelheid

    return voorwerpen_voorhanden_met(zond, wezen, is_goed_genoeg)


def verbruik_goed(zond, wezen, goed, te_verbruiken, hoeveelheid):
    betrekking = zond.geef(goed.goed, te_verbruiken)
    if betrekking.massa <= hoeveelheid:
        print(f"Vernietigen: {hoeveelheid} {goed}")
        zond.vernietig(te_verbruiken)
    else:
        betrekking.massa -= hoeveelheid
        print(f"Verbruiken: {hoeveelheid} {goed} -> {betrekking.massa}")
    return True


def maak_produceerbaar_voorwerp_voorhanden(goed, processen):
    class ProduceerbaarVoorwerpVoorhanden(VoorwerpVoorhanden):
        def __init__(self):
            def is_goed(zond, wat):
                return bool(list(zond.zeef(goed.goed, wat)))
            super().__init__(is_goed)

        def oplossingen(self, zond, wezen):
            resultaat = super().oplossingen(zond, wezen)
            for proces in processen:
                def voer_uit(zond, afteller, wezen):
                    factor = None
                    invoerzaken = []
                    for invoergoed, invoerhoeveelheid in proces.invoeren:
                        nieuwe_factor = hoeveelheid_voorhanden(zond, wezen, invoergoed) / invoerhoeveelheid
                        if factor is None or nieuwe_factor < factor:
                            factor = nieuwe_factor
                    for invoergoed, invoerhoeveelheid in proces.invoeren:
                        hoeveelheid = factor * invoerhoeveelheid
                        mogelijkheden = list(genoeg_goed_voorhanden(zond, wezen, invoergoed, hoeveelheid))
                        assert mogelijkheden
                        te_verbruiken = kies_willekeurig(mogelijkheden)
                        invoerzaken.append((te_verbruiken, hoeveelheid))
                    for uitvoergoed, uitvoerhoeveelheid in proces.uitvoeren:
                        produceer_goed(zond, wezen, uitvoergoed, factor * uitvoerhoeveelheid, invoerzaken)
                    for (te_verbruiken, hoeveelheid), (invoer, _) in zip(invoerzaken, proces.invoeren):
                        verbruik_goed(zond, wezen, invoer, te_verbruiken, hoeveelheid)
                    print(f"Productie {goed} klaar over {factor * proces.tijd}")
                    klaar_over(zond, afteller, wezen, factor * proces.tijd)
                    return True
                resultaat.append((voer_uit, [gedrag_voorhanden(invoer[0]) for invoer in proces.invoeren]))
            return resultaat

        def __str__(self):
            return f"ProduceerbaarVoorwerpVoorhanden({goed}, {processen})"

    return ProduceerbaarVoorwerpVoorhanden
