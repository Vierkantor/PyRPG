"""De lichaamsbouw van mensen."""

from pyrpg.wezen import Lichaamsdeel
from zond import Betrekking, Zaak


def nieuw_deel(zond, zit_aan):
    """Afkorting om een nieuw lichaamsdeel toe te voegen."""
    deel = zond.zaak()
    zond.betrek(Lichaamsdeel, deel=deel, zit_aan=zit_aan)
    return deel


class Vinger(Betrekking):
    """Geef aan dat dit lichaamsdeel een vinger is.

    Verderop willen we de vingers van iemand opzoeken, vandaar deze betrekking.
    """

    wezen = Zaak
    deel = Zaak


def arm(zond, wezen, romp):
    """Afkorting voor een menselijke arm."""
    opperarm = nieuw_deel(zond, romp)
    onderarm = nieuw_deel(zond, opperarm)
    hand = nieuw_deel(zond, onderarm)
    duim = nieuw_deel(zond, hand)
    zond.betrek(Vinger, wezen=wezen, deel=duim)
    wijsvinger = nieuw_deel(zond, hand)
    zond.betrek(Vinger, wezen=wezen, deel=wijsvinger)
    middelvinger = nieuw_deel(zond, hand)
    zond.betrek(Vinger, wezen=wezen, deel=middelvinger)
    ringvinger = nieuw_deel(zond, hand)
    zond.betrek(Vinger, wezen=wezen, deel=ringvinger)
    pink = nieuw_deel(zond, hand)
    zond.betrek(Vinger, wezen=wezen, deel=pink)


def been(zond, romp):
    """Afkorting voor een menselijk been."""
    opperbeen = nieuw_deel(zond, romp)
    onderbeen = nieuw_deel(zond, opperbeen)
    voet = nieuw_deel(zond, onderbeen)
    _grote_teen = nieuw_deel(zond, voet)
    _tweede_teen = nieuw_deel(zond, voet)
    _derde_teen = nieuw_deel(zond, voet)
    _vierde_teen = nieuw_deel(zond, voet)
    _kleine_teen = nieuw_deel(zond, voet)


def bouw_menselijk_lichaam(zond, wezen):
    """Geef het wezen een menselijk lichaam."""
    # Het hoofd is noodzakelijk, de rest hangt eraan.
    hoofd = nieuw_deel(zond, wezen)
    nek = nieuw_deel(zond, hoofd)
    romp = nieuw_deel(zond, nek)
    # Linker- en rechterarm:
    arm(zond, wezen, romp)
    arm(zond, wezen, romp)
    # Linker- en rechterbeen:
    been(zond, romp)
    been(zond, romp)
