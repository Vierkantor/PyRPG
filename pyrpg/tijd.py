"""pyrpg.tijd - Functies om met tijd te werken en te kunnen aftellen.

Tijd wordt weergegeven in generieke eenheden,
en er zijn functies om generieke eenheden te maken uit menselijk bekende eenheden.
Gebeurtenissen zijn aanroepbare objecten (functies) die geen argumenten nemen.
"""

import queue

from pyrpg.eenheid import seconde

# Constanten, voor intern gebruik.
_lengte_minuut = 60
_lengte_uur = 60 * _lengte_minuut
_lengte_dag = 24 * _lengte_uur
_lengte_week = 7 * _lengte_dag


def uit_seconden(aantal_seconden):
    """Zet een aantal seconden om in een generiek tijdsinterval."""
    return aantal_seconden


def uit_minuten(aantal_minuten):
    """Zet een aantal minuten om in een generiek tijdsinterval."""
    return aantal_minuten * _lengte_minuut


def uit_uren(aantal_uren):
    """Zet een aantal uren om in een generiek tijdsinterval."""
    return aantal_uren * _lengte_uur


def uit_dagen(aantal_dagen):
    """Zet een aantal dagen om in een generiek tijdsinterval."""
    return aantal_dagen * _lengte_dag


def uit_weken(aantal_weken):
    """Zet een aantal dagen om in een generiek tijdsinterval."""
    return aantal_weken * _lengte_week


def naar_dagen(tijdsduur):
    """Zet een tijdsduur om in een aantal dagen."""
    return tijdsduur / _lengte_dag


class Afteller:
    """Houdt bij hoeveel tijd er is verstreken en wat wanneer zou moeten gebeuren."""

    def __init__(self):
        self.huidige_tijd = 0
        self.gebeurtenissen_te_gaan = queue.PriorityQueue()

        # Als twee gebeurtenissen op precies hetzelfde tijdstip vallen,
        # kunnen we fouten krijgen omdat de ordening niet gedefinieerd is.
        # Door unieke waarden aan de tupel toe te voegen, voorkomen we dat.
        self.uniciteitsteller = 0

    def nu(self):
        """De huidige tijd voor deze afteller. Tel er een tijdsduur bij op om andere tijdstippen te verkrijgen."""
        return self.huidige_tijd

    def bij_zonsopkomst(self):
        """Geef de tijdsduur tot zonsopkomst."""
        tijd_vandaag = self.huidige_tijd % _lengte_dag
        return _lengte_dag - tijd_vandaag

    def stel_gebeurtenis_in(self, tijdsduur, gebeurtenis):
        """Stel in dat een gebeurtenis over de gegeven hoeveelheid tijd gebeurt."""
        self.gebeurtenissen_te_gaan.put(
            (self.huidige_tijd + seconde(tijdsduur).waarde, self.uniciteitsteller, gebeurtenis)
        )
        self.uniciteitsteller += 1

    def tijd_vooruit(self, tijdsduur):
        """Ga de gegeven hoeveelheid tijd vooruit.

        Gebeurtenissen in de tussenliggende tijd worden afgevuurd.
        """
        nieuwe_tijd = self.huidige_tijd + tijdsduur

        while True:
            # Haal een element uit de queue zolang die voor het nieuwe tijdstip is.
            # Mocht de tijd toch te laat zijn, zetten we het element terug.
            # (TODO: kan dit beter?)
            try:
                tijd, tellertje, gebeurtenis = self.gebeurtenissen_te_gaan.get(
                    block=False
                )
            except queue.Empty:
                # Geen gebeurtenissen te gaan of terug te zetten, dus stop.
                break

            if tijd > nieuwe_tijd:
                # Zet het element terug.
                self.gebeurtenissen_te_gaan.put((tijd, tellertje, gebeurtenis))
                break

            # De gebeurtenis gebeurt op het aangegeven tijdstip.
            self.huidige_tijd = tijd
            gebeurtenis()

        # Alles is gebeurd, dus update naar nieuwe tijdstip.
        self.huidige_tijd = nieuwe_tijd
