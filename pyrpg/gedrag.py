"""pyrpg.gedrag - Definieert een paar generieke gedragingen voor wezens.

Wezens hebben een lijst van doelen die ze waar willen houden.
Denk aan: geen honger hebben, vrienden maken, wereldverovering.
Om de doelen waar te maken, kunnen ze kiezen uit bezigheden.
Denk aan: fruit plukken, naar feestje gaan, een moordaanslag op de president.
Deze bezigheden kunnen zelf weer voorwaarden hebben die werken als doelen op zich.
"""

from dataclasses import dataclass, field
from queue import Empty, PriorityQueue
from typing import Any

from pyrpg.gebouw import Ingang, InGebouw, Thuis, gebouw_van
from pyrpg.inventaris import Vastgehouden, voorhanden
from pyrpg.plek import Begroeiing, Omgeving, Plek, Verbinding, waar_is
from pyrpg.stof import BestaatUit, Steen
from pyrpg.tijd import uit_seconden, uit_uren, uit_weken
from pyrpg.toeval import kies_willekeurig
from pyrpg.voedsel import is_voedsel
from pyrpg.wezen import Hongerniveau, bepaal_hongerniveau, eet_maaltijd
from zond import BestaatNietError, Betrekking, Zaak

# Lijst van prioriteiten, hoger is belangrijker.
levensbelang = 3
noodzakelijk = 2
nuttig = 1
leuk = 0


class OnhaalbaarError(Exception):
    """Een doel is onhaalbaar."""

    def __init__(self, doel, zond, wezen, context):
        super().__init__()
        self.doel = doel
        self.zond = zond
        self.wezen = wezen
        self.context = context


class Doel:
    """Een doel is iets wat een wezen voor elkaar zou willen krijgen.

    Dit is een abstracte klasse: elk doel moet methoden `waargemaakt' en `oplossingen' hebben.

    Zie ook Doelstelling voor de koppeling wezen <-> Doel.
    """

    # Boom van de vorm `(bezigheid, [voorwaarde])' die we af kunnen lopen:
    # een van de bezigheden wil dit wezen doen, door eerst de lijst van voorwaarden te doen.
    bezighedenboom: list

    def __init__(self):
        self.bezighedenboom = None

    def oplossingen(self, zond, wezen):
        """Geef een lijst `[(bezigheid, [voorwaarde])]' om in de bezighedenboom te zetten."""
        raise NotImplementedError("implementeer deze in de kindklassen!")

    def waargemaakt(self, zond, afteller, wezen):
        """Is dit doel (voorlopig) waargemaakt?"""
        raise NotImplementedError("implementeer deze in de kindklassen!")

    def bepaal_bezighedenboom(self, zond, wezen):
        """Vul de waarde van `self.bezighedenboom' in, als die ontbreekt."""
        if self.bezighedenboom is None:
            self.bezighedenboom = self.oplossingen(zond, wezen)
        return self.bezighedenboom

    def eerste_haalbare_stap(self, zond, afteller, wezen):
        """Geef de eerste bezigheid in de bezighedenboom die geen verdere voorwaarden heeft."""
        context = []
        for (bezigheid, voorwaarden) in self.bepaal_bezighedenboom(zond, wezen):
            mogelijk = True
            for voorwaarde in voorwaarden:
                if not voorwaarde.waargemaakt(zond, afteller, wezen):
                    try:
                        return voorwaarde.eerste_haalbare_stap(zond, afteller, wezen)
                    except OnhaalbaarError as e:
                        context.append((voorwaarde, e.context))
                        mogelijk = False
                        break
            if mogelijk:
                return bezigheid
        raise OnhaalbaarError(self, zond, wezen, context)


class Doelstelling(Betrekking):
    """Koppelt een wezen aan een intrinsiek doel.

    Doelen die het wezen kan hebben om andere doelen te vervullen worden via de bezighedenboom
    die onder het Doel hangt.
    """

    handelaar = Zaak
    doel = Doel
    prioriteit = int


def geef_doelstelling(zond, handelaar, doel, *args, prioriteit, **kwargs):
    """Geef de volger een doelstelling bestaande uit `doel(*args, **kwargs)'."""
    zond.betrek(
        Doelstelling,
        doel=doel(*args, **kwargs),
        handelaar=handelaar,
        prioriteit=prioriteit,
    )


def gesorteerde_doelen(zond, wezen):
    """Geef elk Doel van dit wezen, belangrijkste eerst."""
    doelstellingen = zond.zoek(Doelstelling.handelaar, wezen)
    return [
        doelstelling.doel
        for doelstelling in sorted(
            list(doelstellingen),
            reverse=True,  # Hoogste prioriteit eerst
            key=lambda doelstelling: -doelstelling.prioriteit,
        )
    ]


def klaar_over(zond, afteller, wezen, tijdsduur):
    """Het wezen moet na de gegeven tijdsduur opnieuw verzinnen wat te doen."""
    afteller.stel_gebeurtenis_in(
        tijdsduur, lambda: bepaal_wat_te_doen(zond, afteller, wezen)
    )


def dagdromen(zond, afteller, wezen):
    """Het wezen staat een tijdje te dagdromen."""
    klaar_over(zond, afteller, wezen, uit_uren(1))
    return True


def bepaal_wat_te_doen(zond, afteller, wezen):
    """Verzin welke bezigheid het wezen gaat doen om doelen waar te maken."""
    doelen = gesorteerde_doelen(zond, wezen)
    for doel in doelen:
        # Als we er niets aan hoeven te doen, dan doen we er niets aan.
        if doel.waargemaakt(zond, afteller, wezen):
            continue
        bezigheid = doel.eerste_haalbare_stap(zond, afteller, wezen)
        return bezigheid(zond, afteller, wezen)

    # Niks te doen:
    return dagdromen(zond, afteller, wezen)


class Klaar(Doel):
    """Niets te doen."""

    def oplossingen(self, zond, wezen):
        return []

    def waargemaakt(self, zond, afteller, wezen):
        return True


class Volgen(Doel):
    """Volg het gegeven wezen."""

    te_volgen: Zaak

    def __init__(self, te_volgen):
        super().__init__()
        self.te_volgen = te_volgen

    def oplossingen(self, zond, wezen):
        return []

    def waargemaakt(self, zond, afteller, wezen):
        return waar_is(zond, wezen) == waar_is(zond, self.te_volgen)


class HongerStillen(Doel):
    """Zorg ervoor dat we niet verhongeren."""

    def oplossingen(self, zond, wezen):
        def eet_voorhanden(zond, afteller, wezen):
            """Het wezen eet iets indien voorhanden."""
            mogelijkheden = list(
                voorwerpen_voorhanden_met(
                    zond, wezen, lambda zond, voorwerp: is_voedsel(zond, wezen, voorwerp)
                )
            )
            if mogelijkheden:
                maaltijd = kies_willekeurig(mogelijkheden)
                zond.vernietig(maaltijd)
                eet_maaltijd(zond, afteller, wezen)
                klaar_over(zond, afteller, wezen, uit_uren(0.5))
                return True
            return False

        return [
            (
                eet_voorhanden,
                [
                    VoorwerpVoorhanden(
                        lambda zond, voorwerp: is_voedsel(zond, wezen, voorwerp)
                    )
                ],
            )
        ]

    def waargemaakt(self, zond, afteller, wezen):
        return bepaal_hongerniveau(zond, afteller, wezen) not in {
            Hongerniveau.uitgehongerd,
            Hongerniveau.hongerig,
        }


def voorwerpen_voorhanden_met(zond, wezen, voorwaarde):
    """Bepaal alle voorwerpen die dit wezen mee heeft en kan eten."""
    return filter(lambda wat: voorwaarde(zond, wat), voorhanden(zond, wezen))


def voorwerpen_op_plek_met(zond, plek, voorwaarde):
    """Bepaal alle voorwerpen op de plek die voldoen aan een voorwaarde."""
    return filter(lambda wat: voorwaarde(zond, wat), zond.zeef(Plek.waar, plek).waarden(Plek.wat))


class VoorwerpVoorhanden(Doel):
    """Zorg ervoor dat je een voorwerp in handen hebt dat voldoet aan de voorwaarde."""

    def __init__(self, voorwaarde):
        super().__init__()
        self.voorwaarde = voorwaarde

    def oplossingen(self, zond, wezen):
        def pak_op(zond, afteller, wezen):
            """Pak een willekeurig voorwerp dat voldoet aan de voorwaarde."""
            mogelijkheden = list(
                voorwerpen_op_plek_met(zond, waar_is(zond, wezen), self.voorwaarde)
            )
            voorwerp = kies_willekeurig(mogelijkheden)
            zond.onttrek_alle(Plek.wat, voorwerp)
            zond.betrek(Vastgehouden, grijper=wezen, gegrepen=voorwerp)
            klaar_over(zond, afteller, wezen, uit_seconden(1))
            return True

        return [(pak_op, [VoorwerpHier(self.voorwaarde)])]

    def waargemaakt(self, zond, afteller, wezen):
        mogelijkheden = list(voorwerpen_voorhanden_met(zond, wezen, self.voorwaarde))
        return bool(mogelijkheden)


@dataclass(order=True)
class PrioriteitObject:
    """Hulpklasse om alleen de prioriteit te gebruiken bij het sorteren.

    Zie ook de documentatie van `queue.PriorityQueue`.
    """

    prioriteit: int
    object: Any = field(compare=False)


def zoek_pad(zond, wezen, eis):
    """Zoek een pad van de huidige locatie van het wezen naar een plek die voldoet aan `eis`.

    Als het wezen geen `Plek' heeft, kan het ook geen route vinden.

    TEDOEN: iets van een bovengrens op welke afstand we acceptabel vinden?

    :param eis: Een functie die gegeven een plek (van type `Zaak`) bepaalt of het pad daar mag stoppen.
    :return: `False` indien niets gevonden, of een lijst van plekken om te bezoeken, van lengte `≥ 1`.
    """

    # Als het wezen geen `Plek' heeft, kan het ook geen route vinden.
    try:
        beginpunt = zond.geef(Plek.wat, wezen).waar
    except BestaatNietError:
        return False

    # We doen hier een simpele vorm van Dijkstra's zoekalgoritme:
    # we hebben een PriorityQueue van bereikbare knopen, geordend op afstand van beginpunt.
    bereikbaar = PriorityQueue()
    bereikbaar.put(PrioriteitObject(0, beginpunt))
    # En we houden bij wat de beste manier is om in de reeds bereikte knopen te komen.
    bekend = {beginpunt: (0, None)}

    while True:
        try:
            volgende_knoop = bereikbaar.get(False)
        except Empty:
            # Nog geen acceptabel eindpunt gevonden en we kunnen niet verder zoeken, geef het op.
            return False

        huidige_afstand = volgende_knoop.prioriteit
        huidige_plek = volgende_knoop.object

        # Zijn we al klaar?
        if eis(volgende_knoop.object):
            doel = volgende_knoop.object
            break

        # Kijk of we (nieuwe) knopen (efficiënter) kunnen bereiken.
        for verbinding in zond.zoek(Verbinding.van, huidige_plek):
            nieuwe_afstand = huidige_afstand + 1  # TEDOEN: variabele afstanden
            nieuwe_plek = verbinding.naar
            # Kunnen we het efficiënter bereiken?
            if nieuwe_plek not in bekend or nieuwe_afstand < bekend[nieuwe_plek][0]:
                bekend[nieuwe_plek] = (nieuwe_afstand, huidige_plek)
                bereikbaar.put(PrioriteitObject(nieuwe_afstand, nieuwe_plek))

    # Bepaal het pad van `beginpunt` naar `doel` gegeven de bekende buren.
    resultaat = [doel]
    while doel != beginpunt:
        (_afstand, doel) = bekend[doel]
        resultaat.append(doel)
    return resultaat[::-1]


class VolgPad(Doel):
    """Gedrag waarbij het wezen het gegeven pad volgt."""

    def __init__(self, pad):
        super().__init__()
        self.pad = pad

    def waargemaakt(self, zond, afteller, wezen):
        return waar_is(zond, wezen) == self.pad[-1]

    def oplossingen(self, zond, wezen):
        if not self.pad or len(self.pad) == 1:
            return []

        def volg_verbinding(zond, afteller, wezen):
            beginpunt = waar_is(zond, wezen)
            # TEDOEN: indien er meerdere opties zijn, kies de kortste
            verbinding = (
                zond.zeef(Verbinding.van, beginpunt)
                .zeef(Verbinding.naar, self.pad[-1])
                .geef()
            )[0]
            zond.onttrek_alle(Plek.wat, wezen)
            zond.betrek(Plek, wat=wezen, waar=verbinding.naar)
            # TEDOEN: variabele afstanden
            klaar_over(zond, afteller, wezen, uit_seconden(60))
            return True

        return [(volg_verbinding, [VolgPad(self.pad[:-1])])]


class VoorwerpHier(Doel):
    """Zorg ervoor dat je je op dezelfde plek bevindt als een voorwerp dat voldoet aan de voorwaarde."""

    def __init__(self, voorwaarde):
        super().__init__()
        self.voorwaarde = voorwaarde

    def oplossingen(self, zond, wezen):
        pad = zoek_pad(
            zond,
            wezen,
            lambda plek: bool(
                list(voorwerpen_op_plek_met(zond, plek, self.voorwaarde))
            ),
        )
        if pad:
            return [((lambda zond, wezen: True), [VolgPad(pad)])]
        return []

    def waargemaakt(self, zond, afteller, wezen):
        plek = waar_is(zond, wezen)
        te_vinden = list(voorwerpen_op_plek_met(zond, plek, self.voorwaarde))
        return bool(te_vinden)


class PlekHier(Doel):
    """Zorg ervoor dat je op een plek bevindt die voldoet aan de voorwaarde."""

    def __init__(self, voorwaarde):
        super().__init__()
        self.voorwaarde = voorwaarde

    def oplossingen(self, zond, wezen):
        pad = zoek_pad(zond, wezen, self.voorwaarde)
        if pad:
            return [((lambda zond, wezen: True), [VolgPad(pad)])]
        return []

    def waargemaakt(self, zond, afteller, wezen):
        plek = waar_is(zond, wezen)
        return self.voorwaarde(plek)


class ThuisNodig(Doel):
    """Zorg ervoor dat je in een huis woont."""

    def oplossingen(self, zond, wezen):
        def onbewoond(plek):
            return list(zond.zeef(InGebouw.plek, plek)) and not list(
                zond.zeef(InGebouw.plek, plek).bind(InGebouw.gebouw, Thuis.gebouw)
            )

        def is_bouwmateriaal(zond, voorwerp):
            return bool(
                list(
                    zond.zeef(BestaatUit.voorwerp, voorwerp).bind(
                        BestaatUit.stof, Steen.stof
                    )
                )
            )

        def geschikt_voor_bouwen(plek):
            # We gaan ervan uit dat een plek geschikt is om een huis te bouwen
            # als het op dit moment begroeid is.
            # TEDOEN: betere logica
            return list(zond.zoek(Begroeiing.plek, plek))

        def kies_onbewoond_gebouw_uit(zond, afteller, wezen):
            assert onbewoond(waar_is(zond, wezen))
            gebouw = gebouw_van(zond, waar_is(zond, wezen))
            zond.betrek(Thuis, bewoner=wezen, gebouw=gebouw)
            return bepaal_wat_te_doen(zond, afteller, wezen)

        def bouw_en_kies(zond, afteller, wezen):
            huis = zond.zaak()
            kamer = zond.zaak()
            zond.betrek(Omgeving, buitenom=waar_is(zond, wezen), binnenin=huis)
            zond.betrek(Ingang, omhulsel=huis, omhuld=kamer)
            zond.betrek(Thuis, bewoner=wezen, gebouw=huis)
            return klaar_over(zond, afteller, wezen, uit_weken(1))

        return [
            (kies_onbewoond_gebouw_uit, [PlekHier(onbewoond)]),
            (
                bouw_en_kies,
                [VoorwerpVoorhanden(is_bouwmateriaal), PlekHier(geschikt_voor_bouwen)],
            ),
        ]

    def waargemaakt(self, zond, afteller, wezen):
        return bool(list(zond.zoek(Thuis.bewoner, wezen)))
