"""pyrpg.kruising - definieert wat er gebeurt bij het kruisen van wezens."""


from enum import Enum

from pyrpg.opslaan import pyrpg_naar_yaml
from pyrpg.toeval import kies_willekeurig


class Eigenschap:
    """Een eigenschap is een onderdeel van de kruisingsinfo die bepaalde waarden kan aannemen.

    In principe is de opgeslagen waarde het genotype.

    Het geeft aan hoe je van de info van je ouders een kindeigenschap maakt,
    hoe de eigenschap uitgedrukt wordt (oftewel het fenotype),
    en bewaart de onderliggende data.
    """

    @classmethod
    def voor_kind(cls, eigenschap_1, eigenschap_2):
        """Geef de eigenschap van het kind gegeven de eigenschappen van de ouders.

        Deze moet in de kindklasse geïmplementeerd worden.

        :param eigenschap_1: De eigenschap van ouder 1. Mag None zijn.
        :param eigenschap_2: De eigenschap van ouder 2. Mag None zijn.
        :return: De eigenschap van het kind.
        """
        raise NotImplementedError()

    def uitdrukking(self):
        """Geef de uitgedrukte waarde van deze eigenschap.

        Deze moet in de kindklasse geïmplementeerd worden.
        """
        raise NotImplementedError()


class BoolseEigenschap(Eigenschap):
    """Deze eigenschap heeft twee waarden weergegeven als een bool, waarbij True dominant is."""

    def __init__(self, allel_a, allel_b):
        self.allel_a = allel_a
        self.allel_b = allel_b

    @classmethod
    def geef_willekeurig(cls):
        """Geef een BoolseEigenschap met de allelen iid gekozen."""
        allel_a = kies_willekeurig([True, False])
        allel_b = kies_willekeurig([True, False])
        return cls(allel_a, allel_b)

    @classmethod
    def voor_kind(cls, eigenschap_1, eigenschap_2):
        """Geef de eigenschap van het kind gegeven de eigenschappen van de ouders.

        :param eigenschap_1: De eigenschap van ouder 1. Mag None zijn.
        :param eigenschap_2: De eigenschap van ouder 2. Mag None zijn.
        :return: De eigenschap van het kind.
        """

        # False is een goede defaultwaarde, want het is recessief.
        # Je krijgt dus de uitkomst van de andere eigenschap.
        if eigenschap_1:
            allel_1 = kies_willekeurig([eigenschap_1.allel_a, eigenschap_1.allel_b])
        else:
            allel_1 = False
        if eigenschap_2:
            allel_2 = kies_willekeurig([eigenschap_2.allel_a, eigenschap_2.allel_b])
        else:
            allel_2 = False
        return cls(allel_1, allel_2)

    def uitdrukking(self):
        """De uitdrukking van een boolse eigenschap is een bool, waar True dominant is."""
        return self.allel_a or self.allel_b


class KruisingSoort(Enum):
    """De soort van een wezen bepaalt de kruisingsvatbaarheid en algemene lichaamsbouw."""

    mens = "mens"
    eend = "eend"
    kraai = "kraai"
    schaap = "schaap"


@pyrpg_naar_yaml.dumper(KruisingSoort, "KruisingSoort", version=1)
def _dump_KruisingSoort(kruising_soort):
    return kruising_soort.value


@pyrpg_naar_yaml.loader("KruisingSoort", version=1)
def _laad_KruisingSoort(kruising_soort, version=1):
    return KruisingSoort[kruising_soort]


class KruisingInfo:
    """Houdt alle informatie bij om te bepalen wat een kruising doet."""

    def __init__(self, soort, eigenschappen):
        """

        :param eigenschappen: dict van eigenschappen.
        """
        self.soort = soort
        self.eigenschappen = eigenschappen

    @classmethod
    def kruising_mogelijk(cls, info_1, info_2):
        """Geef een bool of de kruising tussen de wezens mogelijk is."""
        return info_1.soort == info_2.soort

    def geef_kruising(self, ander):
        """Geef een KruisingInfo die we krijgen als we twee wezens met de respectievelijke info's kruisen.

        :param self: De KruisingInfo van ouder 1.
        :param ander: De KruisingInfo van ouder 2.
        :return: De KruisingInfo van het kind.
        """
        nieuwe_eigenschappen = {}
        # Doe alle eigenschappen die ouder 1 heeft. Ouder 2 hoeft deze dus niet te hebben.
        for symbool, eigenschap_1 in self.eigenschappen.items():
            eigenschap_2 = ander.eigenschappen.get(symbool)
            nieuwe_eigenschappen[symbool] = type(eigenschap_1).voor_kind(
                eigenschap_1, eigenschap_2
            )

        # Doe alle eigenschappen die alleen ouder 2 heeft.
        for symbool, eigenschap_2 in ander.eigenschappen.items():
            if symbool in self.eigenschappen:
                continue
            nieuwe_eigenschappen[symbool] = type(eigenschap_2).voor_kind(
                None, eigenschap_2
            )

        # Ouder 1 bepaalt de soort.
        return KruisingInfo(self.soort, nieuwe_eigenschappen)

    def bij_ontbreken(self, symbool, waarde_thunk):
        """Indien het symbool ontbreekt in de eigenschappen, zetten we de (gethunkte) waarde daar neer.

        :param symbool: Het symbool dat hoort bij deze Eigenschap.
        :param waarde_thunk: Een gethunkte Eigenschap. Oftewel: waarde_thunk() geeft een eigenschap.
        """
        if symbool not in self.eigenschappen:
            self.eigenschappen[symbool] = waarde_thunk()


@pyrpg_naar_yaml.dumper(KruisingInfo, "KruisingInfo", version=1)
def _dump_KruisingInfo(kruising_info):
    return {"soort": kruising_info.soort, "eigenschappen": kruising_info.eigenschappen}


@pyrpg_naar_yaml.loader("KruisingInfo", version=1)
def _laad_KruisingInfo(kruising_info, version=1):
    return KruisingInfo(kruising_info["soort"], kruising_info["eigenschappen"])
