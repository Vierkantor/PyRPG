#!/usr/bin/env python3
"""pyrpg - Een interactieve wereld in Python."""

# Dit is de opstartmodule, dus alles wat hier gedefinieerd wordt,
# is beschikbaar vanuit de Pythonomgeving bij het opstarten.

import os.path
import sys
from collections import defaultdict

from pyrpg.kruising import KruisingInfo, KruisingSoort
from pyrpg.opslaan import pyrpg_naar_yaml
from pyrpg.plek import Omgeving, Plek
from pyrpg.wezen import Wezen
from zond import Betrekking, Zaak, Zond


class Relatie:
    """Representeert hoe een personage denkt over een ander.

    De leden van deze klasse representeren de soorten relaties die deze wereld modelleert.
    """

    aangetrokken = "aangetrokken"
    handelend = "handelend"
    respect = "respect"
    romance = "romance"
    vriendschap = "vriendschap"

    def __init__(self):
        self.waarden = defaultdict(lambda: 0)

    def wijzig(self, soort, verschil):
        """Stel de relatie bij."""
        self.waarden[soort] += verschil


class Personage(Betrekking):
    """Representeert een wezen met zekere maat van bewustzijn.

    We gaan ervan uit dat alle zaken met deze betrekking ook een Wezen zijn.
    TEDOEN: kunnen we dat programmatisch garanderen?
    """

    wezen = Zaak


@pyrpg_naar_yaml.dumper(Personage, "Personage", version=1)
def _dump_Personage(personage):
    return {"wezen": personage.wezen}


@pyrpg_naar_yaml.loader("Personage", version=1)
def _laad_Personage(personage, version=1):
    return Personage(wezen=personage["wezen"])


def maak_personage(zond, titel):
    """Maak een personage met gegeven titel.

    :param zond: Een Zond-object.
    :param titel: De naam van het personage.
    :return: De Zaak met de personage-informatie.
    """
    personage = zond.zaak()
    zond.betrek(
        Wezen,
        wezen=personage,
        titel=titel,
        kruisinginfo=KruisingInfo(KruisingSoort.mens, {}),
    )
    zond.betrek(Personage, wezen=personage)
    return personage


class Scene:
    """Representeert een of andere gebeurtenis.

    De speler mag kiezen wat er gebeurt uit een paar opties.
    """

    def __init__(self, tekst, keuzes):
        self.tekst = tekst
        self.keuzes = keuzes

    def toon(self, uit=print):
        """Schrijf de volledige tekst van deze scene naar het scherm.

        Voor (o.a.) testbaarheid kun je je eigen uitvoerfunctie meegeven.

        Doet dus niet aan invoer!
        """
        uit(self.tekst)
        for i, keuze in enumerate(self.keuzes):
            uit(" {}: {}".format(i, keuze.tekst_optie))

    def interageer(self, in_=input, uit=print):
        """Voer de scene interactief uit.

        De speler wordt gevraagd om invoer.

        Voor (o.a.) testbaarheid kun je je eigen in- en uitvoerfuncties meegeven.

        Geeft de gekozen optie.
        """
        self.toon(uit=uit)
        while True:
            optie = in_("kies> ")
            try:
                optie_nummer = int(optie)
            except ValueError:
                continue
            try:
                optie_gekozen = self.keuzes[optie_nummer]
            except IndexError:
                continue
            return optie_gekozen


class Optie:
    """Representeert een optie waar verder niets speciaals aan is."""

    def __init__(self, tekst_optie, gevolg):
        self.tekst_optie = tekst_optie
        self.gevolg = gevolg


class Antwoord:
    """Representeert een optie in een keuze die in de vorm van een antwoord op een vraag is."""

    def __init__(self, tekst, gevolg):
        self.tekst = tekst
        self.gevolg = gevolg

    @property
    def tekst_optie(self):
        """Geef de tekst die hoort bij deze optie."""
        return '"{}"'.format(self.tekst)


def maak_wereld(zond):
    """Geef de wijzigbare dingen in de interactieve wereld.

    Het idee van deze functie is dat je bij het testen deze functie herhaaldelijk kan uitvoeren
    en steeds verse data hebt.
    """
    # pylint: disable=unused-variable

    wereld = zond.zaak()
    weiland = zond.zaak()
    zond.betrek(Omgeving, buitenom=wereld, binnenin=weiland)

    speler = zond.zaak()
    zond.betrek(
        Wezen,
        wezen=speler,
        titel="de speler",
        kruisinginfo=KruisingInfo(KruisingSoort.mens, {}),
    )
    zond.betrek(Personage, wezen=speler)
    zond.betrek(Plek, wat=speler, waar=weiland)

    Parvuurnen = zond.zaak()
    zond.betrek(
        Wezen,
        wezen=Parvuurnen,
        titel="Parvuurnen",
        kruisinginfo=KruisingInfo(KruisingSoort.mens, {}),
    )
    zond.betrek(Personage, wezen=Parvuurnen)
    zond.betrek(Plek, wat=Parvuurnen, waar=weiland)

    def wat_een_mooie_schapen():
        """Gevolg behorend bij zeggen dat de schapen die Parvuurnen geleidt, mooi zijn."""
        Parvuurnen.wijzig_relatie(speler, Relatie.vriendschap, +1)
        return Scene(
            """Parvuurnen glimlacht even.
"dankjewel :) ik geleid deze schapen al een tijdje. het is goed werk als je klaar bent met avonturen. en wie ben jij, vreemdeling?"
je kijkt nog eens goed rond. dichtbij zitten in de weide schapen wat te herkauwen, verderop stroomt een beekje langzaam naar beneden. aan de overkant van dat beekje staat een stenen huisje. geen van dit komt je enigszins bekend voor. inderdaad, wie ben je eigenlijk?
            """,
            [
                Antwoord("nu je het zegt, geen idee eigenlijk :S", None),
                Antwoord("beter als ik dat niet zeg.", None),
            ],
        )

    def niet_zo_slimme_schapen():
        """Gevolg behorend bij zeggen dat de schapen die Parvuurnen geleidt, niet zo slim zijn."""
        Parvuurnen.wijzig_relatie(speler, Relatie.vriendschap, -1)

    openingsscene = Scene(
        """zwart.
"bèèèh"
je opent je ogen; blauw.
"hallo daar!"
je beweegt je armen; het lijkt uiteindelijk te werken.
"blèèèh?"
het lukt je om overeind te komen. een schaap bekijkt je even met interesse tot het besluit dat je geen gras bent, en dus helemaal niet interessant bent.
"hoi hallo, mijn naam is Parvuurnen!"
de stem is afkomstig van iemand die op je af komt gewandeld. het lange grijswitte haar van Parvuurnen doet hen verdacht lijken op de schapen die in dit weiland staan.
"gegroet, vreemdeling. wat vind je van de schapen?"
        """,
        [
            Antwoord("wat een mooie schapen :D", wat_een_mooie_schapen),
            Antwoord(
                "oh die? ze zien er eigenlijk niet zo slim uit :P",
                niet_zo_slimme_schapen,
            ),
        ],
    )

    return locals()


zond = Zond()
interactieve_wereld = maak_wereld(zond)


def geimporteerde_modules():
    """Geef een iterator met modules die zijn geïmporteerd.

    Code is geïnspireerd op werkzeug._reloader.
    """
    # we doen `yield from list(...)' om te voorkomen dat we itereren over een gewijzigde waarde
    for module in list(sys.modules.values()):
        if module is not None:
            yield module


def geimporteerde_bestanden():
    """Geef een iterator met bestanden die zijn geïmporteerd.

    Merk op dat dit alleen bestanden geeft die daadwerkelijk bestaan,
    niet zomaar alle bestandsnamen.
    Bestandsnamen zijn mogelijk relatief (bijvoorbeeld __main__).

    Code is geïnspireerd op werkzeug._reloader.
    """
    for module in geimporteerde_modules():
        bestandsnaam = getattr(module, "__file__", None)
        if not bestandsnaam:
            continue
        # voor een map importeren we de __init__.py
        if os.path.isdir(bestandsnaam):
            bestandsnaam_init = os.path.join(bestandsnaam, "__init__.py")
            if os.path.exists(bestandsnaam_init):
                bestandsnaam = bestandsnaam_init
        # TODO: werkzeug gaat ook mappen omhoog als het bestand niet (meer) bestaat,
        # en controleert op .pyc en .pyo.
        # Willen we dat?
        yield bestandsnaam


# Map van bestandsnamen naar de tijd dat wij zagen dat ze voor het laatst zijn aangepast.
_laatst_aangepast_per_bestand = {}


def spoor_wijzigingen_op():  # pragma: no cover
    """Geef een iterator van gewijzigde bestanden.

    Code is geïnspireerd op werkzeug._reloader.
    """
    # TODO: dit kan met inotify misschien?
    for bestandsnaam in geimporteerde_bestanden():
        try:
            mtime = os.stat(bestandsnaam).st_mtime
        except OSError:
            # het bestand bestaat niet (meer?) dus negeren die hap
            continue
        vorig_aangepast = _laatst_aangepast_per_bestand.get(bestandsnaam)
        if vorig_aangepast is not None and vorig_aangepast < mtime:
            return True
        _laatst_aangepast_per_bestand[bestandsnaam] = mtime
    return False
