"""pyrpg.stof - Beschrijving van substanties."""

from pyrpg.eenheid import Grootheid, graad_celsius
from zond import Betrekking, Zaak


class BestaatUit(Betrekking):
    """Geef aan dat de zaak bestaat uit de gegeven stof."""

    voorwerp = Zaak
    stof = Zaak


class Faseovergang(Betrekking):
    """Geef smeltpunt en kookpunt aan van een voorwerp."""

    voorwerp = Zaak
    smeltpunt = Grootheid
    kookpunt = Grootheid


def maak_van_olijfolie(zond, voorwerp):
    """Stel in dat het voorwerp gemaakt is van olijfolie."""
    # Bron: Wikipedia; https://en.wikipedia.org/w/index.php?title=Olive_oil&oldid=866200597
    zond.betrek(
        Faseovergang,
        voorwerp=voorwerp,
        smeltpunt=graad_celsius(-6),
        kookpunt=graad_celsius(700),
    )


class Steen(Betrekking):
    """Geef aan dat deze stof een soort steen is."""

    stof = Zaak
