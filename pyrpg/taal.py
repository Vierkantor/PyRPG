"""pyrpg.taal - Helpt je om met menselijke taal om te gaan.

De bedoeling is dat de objecten in deze module net werken als een str,
zodat je makkelijk je code 'slimmer' kunt maken.
"""


def opsomming(lijstje):
    """Geef een lijstje str's weer als opsomming.

    Voorbeelden:
        [] -> ''
        ['aap'] -> 'aap'
        ['aap', 'noot'] -> 'aap en noot'
        ['aap', 'noot', 'mies'] -> 'aap, noot en mies'
    """
    if not lijstje:
        return ""
    if len(lijstje) == 1:
        return lijstje[0]

    init = lijstje[:-1]
    laatste = lijstje[-1]
    return f'{", ".join(init)} en {laatste}'
