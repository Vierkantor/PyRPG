"""pyrpg.eenheden - Eenheden en grootheden die verwarring zouden moeten voorkomen."""


class KanNietOmrekenen(Exception):
    """Gegooid als we proberen te rekenen met eenheden die niet overweg kunnen."""


class Eenheid:
    """Een eenheid geeft aan hoe we een bepaalde waarde meten."""

    def __init__(self, symbool):
        self.symbool = symbool
        # Geeft hoe we een andere eenheid omrekenen naar deze.
        self.omrekeningen = {}

    def __call__(self, hoeveelheid):
        """Geef de hoeveelheid (getal of Grootheid) weer als Grootheid met deze Eenheid."""
        if isinstance(hoeveelheid, Grootheid):
            # Dezelfde eenheid: niets om om te rekenen!
            if self == hoeveelheid.eenheid:
                return hoeveelheid

            # Reken de waarde om naar deze eenheid.
            try:
                omrekening = self.omrekeningen[hoeveelheid.eenheid]
            except KeyError as e:
                raise KanNietOmrekenen(self, hoeveelheid.eenheid) from e
            return Grootheid(omrekening(hoeveelheid.waarde), self)

        return Grootheid(hoeveelheid, self)

    def __mul__(self, other):
        """Maak een nieuwe Eenheid die een veelvoud van deze voorstelt.

        Let op dat het symbool niet echt zinnig is:
        die wil je waarschijnlijk zelf nog instellen.
        """
        if other == 0:
            raise ValueError("Je kan geen 0-voud van een Eenheid nemen")

        veelvoud = type(self)(f"{other} * {self.symbool}")
        for eenheid, omrekening in self.omrekeningen.items():
            veelvoud.omrekeningen[eenheid] = lambda x: omrekening(x) * other
            eenheid.omrekeningen[veelvoud] = lambda x: eenheid.omrekeningen[self](x / other)
        veelvoud.omrekeningen[self] = lambda x: x * other
        self.omrekeningen[veelvoud] = lambda x: x / other
        return veelvoud

    def __rmul__(self, other):
        """Maak een nieuwe Eenheid die een veelvoud van deze voorstelt.

        Let op dat het symbool niet echt zinnig is:
        die wil je waarschijnlijk zelf nog instellen.
        """
        return self.__mul__(other)

    def __str__(self):
        return self.symbool

    def __repr__(self):
        return f"Eenheid({self.symbool})"


class Grootheid:
    """Een grootheid is een waarde en een eenheid.

    Je kan alleen met grootheden rekenen die hetzelfde meten.
    """

    def __init__(self, waarde, eenheid):
        self.waarde = waarde
        self.eenheid = eenheid

    def __eq__(self, other):
        try:
            other = self.eenheid(other)
        except KanNietOmrekenen:
            return False

        return self.waarde == other.waarde

    def __le__(self, other):
        try:
            other = self.eenheid(other)
        except KanNietOmrekenen:
            return False

        return self.waarde <= other.waarde

    def __rmul__(self, other):
        if isinstance(other, Grootheid):
            return Grootheid(self.waarde * other.waarde, self.eenheid * other.eenheid)
        else:
            return Grootheid(self.waarde * other, self.eenheid)

    def __mul__(self, other):
        if isinstance(other, Grootheid):
            return Grootheid(self.waarde * other.waarde, self.eenheid * other.eenheid)
        else:
            return Grootheid(self.waarde * other, self.eenheid)

    def __truediv__(self, other):
        try:
            other = self.eenheid(other)
        except KanNietOmrekenen:
            return False

        return self.waarde / other.waarde

    def __repr__(self):
        return f"{self.eenheid.symbool}({self.waarde})"

    def __str__(self):
        return f"{str(self.waarde)} {self.eenheid.symbool}"


# Temperatuur
graad_celsius = Eenheid("°C")
kelvin = Eenheid("K")
graad_celsius.omrekeningen[kelvin] = lambda x: x - 273.15
kelvin.omrekeningen[graad_celsius] = lambda x: x + 273.15

# Afstand
meter = Eenheid("m")

# Tijd
seconde = Eenheid("s")
minuut = 60 * seconde
uur = 60 * minuut

# Massa
gram = Eenheid("g")
kilo = 1000 * gram
