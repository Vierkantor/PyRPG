"""pyrpg.effect - Definieert een generieke Effect-klasse.

Deze klasse representeert (bovennatuurlijke) effecten die op spelobjecten werken.
Denk hierbij aan dingen als "personage is slaperig" of "lichaamsdeel is versteend".
"""

from zond import Betrekking, Zaak


class Effect(Betrekking):
    """Generieke effectklasse die alleen een beschrijving heeft.

    Nuttig als je puur cosmetische effecten wilt representeren,
    of als ouderklasse voor ingewikkeldere effecten.
    """

    onderwerp = Zaak
    beschrijving = str


class EffectVerdwijnt:
    """Gebeurtenis die een gegeven effect weghaalt.

    Nuttig om tijdelijke effecten te maken:
    maak een effect en stel EffectVerdwijnt als gebeurtenis in.
    """

    def __init__(self, zond, effect):
        # TEDOEN: kunnen we dit zonder zond te hoeven onthouden?
        self.zond = zond
        self.effect = effect

    def __call__(self):
        self.zond.onttrek(self.effect)
