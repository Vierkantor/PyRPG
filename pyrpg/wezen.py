"""pyrpg.wezen - Definieert een generieke Wezen-klasse en een aantal soorten."""

from enum import Enum

from pyrpg.kruising import KruisingInfo
from pyrpg.opslaan import pyrpg_naar_yaml
from pyrpg.tijd import naar_dagen, uit_dagen
from zond import Betrekking, Zaak


class Wezen(Betrekking):
    """Een (al dan niet levende) deelnemer aan het verhaal.

    Onder andere dieren, monsters en personages zijn wezens.
    Ze hebben allemaal een titel (een of andere omschrijving),
    en een gedrag (om keuzes te maken).
    """

    wezen = Zaak
    titel = str
    kruisinginfo = KruisingInfo


@pyrpg_naar_yaml.dumper(Wezen, "Wezen", version=1)
def _dump_Wezen(wezen):
    return {
        "wezen": wezen.wezen,
        "titel": wezen.titel,
        "kruisinginfo": wezen.kruisinginfo,
    }


@pyrpg_naar_yaml.loader("Wezen", version=1)
def _laad_Wezen(wezen, _version=1):
    return Wezen(
        wezen=wezen["wezen"], titel=wezen["titel"], kruisinginfo=wezen["kruisinginfo"]
    )


class Overleden(Betrekking):
    """Geeft aan dat een wezen overleden is."""

    wezen = Zaak
    tijdstip = float
    reden = str


def dood_wezen(zond, afteller, wezen, reden):
    """Veroorzaak het overlijden van een wezen om gegeven reden."""
    zond.betrek(Overleden, wezen=wezen, tijdstip=afteller.nu(), reden=reden)


class Lichaamsdeel(Betrekking):
    """Een wezen heeft een lichaam opgebouwd uit lichaamsdelen.

    Elk lichaamsdeel zit vast aan een ander lichaamsdeel,
    of aan het wezen.
    Als een lichaamsdeel losraakt, dan raakt ook alles los dat daaraan vastzit.
    """

    deel = Zaak
    zit_aan = Zaak


class KanNietKruisen(Exception):
    """Wordt gegooid als je onkruisbare wezens probeert te kruisen."""

    def __init__(self, wezen_1, wezen_2):
        """

        :type wezen_1: Wezen
        :type wezen_2: Wezen
        """
        super().__init__(f"Kan {wezen_1} en {wezen_2} niet kruisen")


def kruis(zond, wezen_1, wezen_2, titel):
    """Maak een nieuw wezen dat de kruising is van de gegeven twee wezens.

    De volgorde kan uitmaken, dus kruis(wezen_1, wezen_2) kan iets heel anders doen dan kruis(wezen_2, wezen_1)

    De plek van het nieuwe wezen is dezelfde plek als wezen 1.

    :param wezen_1: Het eerste wezen om te kruisen.
    :type wezen_1: Wezen
    :param wezen_2: Het tweede wezen om te kruisen.
    :type wezen_2: Wezen
    :type titel: str

    :rtype: Zaak
    """

    if not KruisingInfo.kruising_mogelijk(wezen_1.kruisinginfo, wezen_2.kruisinginfo):
        raise KanNietKruisen(wezen_1, wezen_2)

    wezen = zond.zaak()
    zond.betrek(
        Wezen,
        wezen=wezen,
        titel=titel,
        kruisinginfo=wezen_1.kruisinginfo.geef_kruising(wezen_2.kruisinginfo),
    )
    return wezen


class VoedingNodig(Betrekking):
    """Geef aan dat een wezen moet eten om te overleven."""

    wezen = Zaak
    laatst_gegeten = (
        float  # Tijdstip waarop dit wezen voor het laatst een volledige maaltijd had.
    )


class Hongerniveau(Enum):
    """Hoeveel honger dit wezen heeft. Hangt af van hoe lang dit wezen niet gegeten heeft, zie `bepaal_hongerniveau'."""

    vol = "vol"
    trek = "trek"
    hongerig = "hongerig"
    uitgehongerd = "uitgehongerd"
    hongerdood = "hongerdood"


def bepaal_hongerniveau(zond, afteller, wezen):
    """Hoeveel honger heeft het wezen nu?

    :rtype Hongerniveau:
    """
    dagen_niet_gegeten = naar_dagen(
        afteller.nu() - zond.geef(VoedingNodig.wezen, wezen).laatst_gegeten
    )
    if dagen_niet_gegeten < 0.5:
        return Hongerniveau.vol
    if dagen_niet_gegeten < 1:
        return Hongerniveau.trek
    if dagen_niet_gegeten < 7:
        return Hongerniveau.hongerig
    if dagen_niet_gegeten < 21:
        return Hongerniveau.uitgehongerd
    return Hongerniveau.hongerdood


def stel_eetgebeurtenissen_in(zond, afteller, wezen):
    """Zorg ervoor dat een wezen dat moet eten, problemen krijgt als het niet eet."""

    def sterf_de_hongerdood():
        if bepaal_hongerniveau(zond, afteller, wezen) != Hongerniveau.hongerdood:
            return
        dood_wezen(zond, afteller, wezen, "stierf de hongerdood")

    afteller.stel_gebeurtenis_in(uit_dagen(21), sterf_de_hongerdood)


def eet_maaltijd(zond, afteller, wezen):
    """Handel de gevolgen af van het eten van een maaltijd door een wezen."""
    voeding = zond.geef(VoedingNodig.wezen, wezen)
    voeding.laatst_gegeten = afteller.nu()
    stel_eetgebeurtenissen_in(zond, afteller, wezen)
