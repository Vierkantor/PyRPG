"""pyrpg.__main__ - Opstarten van pyrpg."""

import code

from pyrpg import interactieve_wereld, spoor_wijzigingen_op


def lees_commando(prompt=""):
    """Lees een Python-commando in.

    Geeft ook wat informatie aan de gebruiker zoals of er bestanden zijn gewijzigd.
    """
    if spoor_wijzigingen_op():
        print("LET OP: bestanden zijn gewijzigd")
    return input(prompt)


print(
    """Welkom bij PyRPG!
Het idee achter PyRPG is dat je kan spelen en bouwen aan een interactieve wereld.
De interface is programmatisch, dus we starten bij deze een Pythonomgeving op.
Veel plezier!
"""
)


code.interact(local=interactieve_wereld, readfunc=lees_commando)
