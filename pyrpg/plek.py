"""pyrpg.plek - Het verschil tussen 'hier' en 'niet hier'."""
from enum import Enum

from pyrpg.opslaan import pyrpg_naar_yaml
from zond import BestaatNietError, Betrekking, Zaak


class Omgeving(Betrekking):
    """Plekken in een omgeving hebben gemeenschappelijke eigenschappen.

    Bijvoorbeeld:
    er is een omgeving voor de wereld (regelt o.a. dat de zon opkomt),
    waarbinnen eentje voor een gebied met vergelijkbaar klimaat (regelt o.a. termperatuur),
    waarbinnen eentje voor een dorp,
    waarbinnen eentje voor een huis,
    die een plek bevat voor alle kamers.
    """

    binnenin = Zaak
    buitenom = Zaak


@pyrpg_naar_yaml.dumper(Omgeving, "Omgeving", version=1)
def _dump_omgeving(omgeving):
    return {"binnenin": omgeving.binnenin, "buitenom": omgeving.buitenom}


@pyrpg_naar_yaml.loader("Omgeving", version=1)
def _laad_omgeving(omgeving, version=1):
    return Omgeving(binnenin=omgeving["binnenin"], buitenom=omgeving["buitenom"])


class Plek(Betrekking):
    """Plekken beschrijven waar wezens zich bevinden.

    Het `waar' van een plek is bijvoorbeeld:
     - een kamer in een huis
     - een weiland met schaapjes
     - een afstand langs de weg tussen twee dorpen
     - bovenin de takken van een grote boom
    (Het komt ongeveer overeen met een `room' in Inform.)
    """

    wat = Zaak
    waar = Zaak


class NergensError(Exception):
    def __init__(self, wat):
        self.wat = wat


def waar_is(zond, wat):
    """Waar bevindt het gegeven wezen zich?

    :raises NergensError: als het gegeven wezen geen toegewezen locatie heeft"""
    try:
        return zond.geef(Plek.wat, wat).waar
    except BestaatNietError as e:
        raise NergensError(wat) from e


@pyrpg_naar_yaml.dumper(Plek, "Plek", version=1)
def _dump_plek(plek):
    return {"wat": plek.wat, "waar": plek.waar}


@pyrpg_naar_yaml.loader("Plek", version=1)
def _laad_plek(plek, version=1):
    return Plek(wat=plek["wat"], waar=plek["waar"])


class Richting(Enum):
    """Een richting om je op te bewegen."""

    noord = "noord"
    zuid = "zuid"
    west = "west"
    oost = "oost"
    op = "op"
    neer = "neer"
    in_ = "in"
    uit = "uit"

    @property
    def achteruit(self):
        return _achteruit[self]


_achteruit = {
    Richting.noord: Richting.zuid,
    Richting.zuid: Richting.noord,
    Richting.west: Richting.oost,
    Richting.oost: Richting.west,
    Richting.op: Richting.neer,
    Richting.neer: Richting.op,
    Richting.in_: Richting.uit,
    Richting.uit: Richting.in_,
}


class Verbinding(Betrekking):
    """Verbindingen tussen plekken geven de wezens die zich daar bevinden de mogelijkheid om van de ene naar de andere plek te gaan.

    Over het algemeen wil je dat deze verbindingen symmetrisch zijn: bij elke `Verbinding(van=plek1, naar=plek2, richting=richting)' hoort ook een `Verbinding(van=plek2, naar=plek1, richting=richting.achteruit)'.
    Zo niet, dan krijg je éénrichtingverkeer.
    De klassemethode `Verbinding.betrek_symmetrisch' zorgt vanzelf voor deze symmetrische verbindingen.
    """

    van = Zaak
    naar = Zaak
    richting = Richting

    @classmethod
    def betrek_symmetrisch(cls, zond, van, naar, richting):
        zond.betrek(cls, van=van, naar=naar, richting=richting)
        zond.betrek(cls, van=naar, naar=van, richting=richting.achteruit)


class Begroeiingssoort(Enum):
    """Geeft aan wat voor soort planten op een plek te vinden zijn."""

    kaal = "kaal"
    dun_onderhout = "dunne-onderhout"
    dicht_onderhout = "dichte-onderhout"
    bomen = "bomen"
    grote_bomen = "grote-bomen"


class Begroeiing(Betrekking):
    """De begroeiing op een plek/omgeving geeft aan wat voor soort planten daar te vinden zijn."""

    plek = Zaak
    begroeiing = Begroeiingssoort
