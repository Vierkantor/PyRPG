"""test_eenheid.py - Tests voor het rekenen met eenheden."""
import pytest
from hypothesis import given
from hypothesis import strategies as st

from pyrpg.eenheid import Eenheid


def test_nulvoud():
    """Een nulvoud van een Eenheid kan niet."""
    eenheid = Eenheid("smoot")
    with pytest.raises(ValueError):
        nulvoud = eenheid * 0


@given(
    st.fractions().filter(lambda x: x != 0), st.fractions(),
)
def test_bijectie_veelvoud(factor, waarde):
    """Het omzetten tussen een veelvoud en het origineel is een bijectie."""
    orig_eenheid = Eenheid("origineel")
    veel_eenheid = orig_eenheid * factor

    origineel = orig_eenheid(waarde)
    heen = veel_eenheid(origineel)
    terug = orig_eenheid(heen)
    # Voorkom gezeur over afrondfouten.
    origineel.waarde = pytest.approx(origineel.waarde)
    assert origineel == terug

    origineel = veel_eenheid(waarde)
    heen = orig_eenheid(origineel)
    terug = veel_eenheid(heen)
    # Voorkom gezeur over afrondfouten.
    origineel.waarde = pytest.approx(origineel.waarde)
    assert origineel == terug
