"""test_taal - tests voor pyrpg.taal"""


from pyrpg.taal import opsomming


def test_voorbeelden_opsomming():
    """Opsommingen van een paar voorbeeldlijsten zien er als volgt uit:"""
    assert opsomming([]) == ""
    assert opsomming(["aap"]) == "aap"
    assert opsomming(["aap", "noot"]) == "aap en noot"
    assert opsomming(["aap", "noot", "mies"]) == "aap, noot en mies"
