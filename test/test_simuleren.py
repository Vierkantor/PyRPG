"""test_simuleren.py - Test dat het aanmaken en simuleren van een wereld voor langere tijd werkt."""

from pyrpg import maak_wereld
from pyrpg.tijd import Afteller, uit_dagen
from zond import Zond


def test_jaar_simuleren():
    """Maak een nieuwe wereld en simuleer het voor 1 jaar, zonder fouten."""
    zond = Zond()
    afteller = Afteller()
    maak_wereld(zond)

    # Zorg ervoor dat er geen fouten gebeuren:
    afteller.tijd_vooruit(uit_dagen(365))
