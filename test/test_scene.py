"""test_scene.py - tests voor Scene-objecten"""

import pyrpg


class InvoerKiezer:
    """Houdt een lijst bij van uitkomsten van aanroepen van dit object."""

    def __init__(self, uitkomsten):
        self.uitkomsten = uitkomsten
        self.iterator = iter(self.uitkomsten)

    def __call__(self, _prompt):
        return next(self.iterator)


class UitvoerOnthouder:
    """Houdt een lijst bij met argumenten van aanroepen naar dit object."""

    def __init__(self):
        self.uitvoer = []

    def __call__(self, regel):
        self.uitvoer.append(regel)


def test_scene_tonen():
    """Een scene met gegeven tekst toont die ook."""
    tekst_scene = "generieke tekst"

    # maak een scene
    scene = pyrpg.Scene(tekst_scene, [])

    # sla de uitvoer op
    uit = UitvoerOnthouder()

    # controleer de uitvoer
    scene.toon(uit)
    assert tekst_scene in uit.uitvoer


def test_keuzes_tonen():
    """Een scene met een keuze toont die ook."""

    # maak een scene
    keuze = pyrpg.Optie("generieke keuze", None)
    scene = pyrpg.Scene("", [keuze])

    # sla de uitvoer op
    uit = UitvoerOnthouder()

    # controleer de uitvoer
    scene.toon(uit)
    gevonden = False
    for regel in uit.uitvoer:
        gevonden = gevonden or regel.find(keuze.tekst_optie) is not None
    assert gevonden


def test_keuze_maken():
    """Je kan interactief een keuze maken."""
    # maak een scene
    keuze = pyrpg.Optie("generieke keuze", None)
    scene = pyrpg.Scene("", [keuze])

    def uit(_):
        """Negeert uitvoer."""
        pass

    # controleer de uitvoer
    gekozen = scene.interageer(InvoerKiezer(["0"]), uit)
    assert gekozen == keuze


def test_onzinnige_keuze_maken():
    """Als je keuze ongeldig is, moet je nog eens proberen."""
    # maak een scene
    keuze = pyrpg.Optie("generieke keuze", None)
    scene = pyrpg.Scene("", [keuze])

    def uit(_):
        """Negeert uitvoer."""
        pass

    # controleer de uitvoer
    gekozen = scene.interageer(InvoerKiezer(["onzin", "37", "0"]), uit)
    assert gekozen == keuze
