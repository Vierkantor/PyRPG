"""test_honger.py - Testfuncties voor honger en eten."""

import pytest

from pyrpg.gedrag import (
    HongerStillen,
    bepaal_wat_te_doen,
    geef_doelstelling,
    levensbelang,
)
from pyrpg.inventaris import Vastgehouden
from pyrpg.plek import Plek
from pyrpg.tijd import Afteller, uit_dagen, uit_uren
from pyrpg.voedsel import Voedsel, eten_voorhanden, is_voedsel
from pyrpg.wezen import (
    Hongerniveau,
    Overleden,
    VoedingNodig,
    bepaal_hongerniveau,
    eet_maaltijd,
    stel_eetgebeurtenissen_in,
)
from zond import Zond


def test_vol_na_eten():
    """Direct na het eten van een maaltijd zit je vol."""
    zond = Zond()
    afteller = Afteller()
    speler = zond.zaak()
    zond.betrek(VoedingNodig, wezen=speler, laatst_gegeten=afteller.nu())

    eet_maaltijd(zond, afteller, speler)

    assert bepaal_hongerniveau(zond, afteller, speler) == Hongerniveau.vol


def test_honger_krijgen():
    """Na een dag niet eten krijg je honger."""
    zond = Zond()
    afteller = Afteller()
    speler = zond.zaak()
    zond.betrek(VoedingNodig, wezen=speler, laatst_gegeten=afteller.nu())

    afteller.tijd_vooruit(uit_dagen(1))

    assert bepaal_hongerniveau(zond, afteller, speler) == Hongerniveau.hongerig


def test_overlijden_na_niet_eten():
    """Als je dagen lang niets eet, verhonger je."""
    zond = Zond()
    afteller = Afteller()
    speler = zond.zaak()
    zond.betrek(VoedingNodig, wezen=speler, laatst_gegeten=afteller.nu())

    eet_maaltijd(zond, afteller, speler)
    afteller.tijd_vooruit(uit_dagen(30))

    assert bepaal_hongerniveau(zond, afteller, speler) == Hongerniveau.hongerdood
    assert zond.geef(Overleden.wezen, speler)


def test_eten_voorhanden():
    """Als je voedsel vasthoudt, telt dat als eten voorhanden."""
    zond = Zond()
    speler = zond.zaak()
    taart = zond.zaak()
    zond.betrek(Voedsel, voorwerp=taart)
    zond.betrek(Vastgehouden, grijper=speler, gegrepen=taart)

    assert taart in eten_voorhanden(zond, speler)


def test_eten_bij_honger():
    """Als je honger hebt, en dan eet, dan heb je geen honger meer."""
    zond = Zond()
    afteller = Afteller()
    speler = zond.zaak()
    zond.betrek(VoedingNodig, wezen=speler, laatst_gegeten=afteller.nu())
    afteller.tijd_vooruit(uit_dagen(1))
    taart = zond.zaak()
    zond.betrek(Voedsel, voorwerp=taart, voedingswaarde=1)
    zond.betrek(Vastgehouden, grijper=speler, gegrepen=taart)
    geef_doelstelling(zond, speler, HongerStillen, prioriteit=levensbelang)

    assert bepaal_wat_te_doen(zond, afteller, speler)

    afteller.tijd_vooruit(uit_uren(1))

    assert bepaal_hongerniveau(zond, afteller, speler) == Hongerniveau.vol


def test_kan_zelf_niet_eten():
    """Een wezen telt niet als hun eigen eten."""
    zond = Zond()
    afteller = Afteller()
    speler = zond.zaak()
    zond.betrek(VoedingNodig, wezen=speler, laatst_gegeten=afteller.nu())
    taart = zond.zaak()
    zond.betrek(Voedsel, voorwerp=taart, voedingswaarde=1)

    assert not is_voedsel(zond, speler, speler)


def test_eten_zoeken_zelfde_plek():
    """Als je honger hebt, en er is wat eten op dezelfde plek als je staat, dan kun je dat eten."""
    zond = Zond()
    afteller = Afteller()
    speler = zond.zaak()
    zond.betrek(VoedingNodig, wezen=speler, laatst_gegeten=afteller.nu())
    kamer = zond.zaak()
    zond.betrek(Plek, wat=speler, waar=kamer)
    afteller.tijd_vooruit(uit_dagen(1))
    taart = zond.zaak()
    zond.betrek(Voedsel, voorwerp=taart, voedingswaarde=1)
    zond.betrek(Plek, wat=taart, waar=kamer)
    geef_doelstelling(zond, speler, HongerStillen, prioriteit=levensbelang)

    assert bepaal_wat_te_doen(zond, afteller, speler)
    # De speler moet de taart oppakken.
    assert list(
        zond.zeef(Vastgehouden.gegrepen, taart).zeef(Vastgehouden.grijper, speler)
    )

    afteller.tijd_vooruit(uit_uren(1))

    # En even later op hebben gegeten.
    assert bepaal_hongerniveau(zond, afteller, speler) == Hongerniveau.vol
