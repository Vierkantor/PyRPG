"""test_temperatuur.py - Tests voor het rekenen met temperaturen."""
import pytest
from hypothesis import given
from hypothesis import strategies as st

from pyrpg.eenheid import KanNietOmrekenen, graad_celsius, kelvin, meter


def test_celsius_uit_kelvin():
    """Het absolute nulpunt is 0 Kelvin, en -273.15 graad Celsius."""
    nulpunt = kelvin(0)
    assert graad_celsius(nulpunt).waarde == -273.15


@given(st.floats(allow_nan=False))
def test_bijectie_celsius_kelvin(waarde):
    """Het omzetten tussen celsius en kelvin is een bijectie."""
    als_kelvin = kelvin(waarde)
    als_celsius = graad_celsius(als_kelvin)
    weer_als_kelvin = kelvin(als_celsius)
    # Voorkom gezeur over afrondfouten.
    als_kelvin.waarde = pytest.approx(als_kelvin.waarde)
    assert als_kelvin == weer_als_kelvin

    als_celsius = graad_celsius(waarde)
    als_kelvin = kelvin(als_celsius)
    weer_als_celsius = graad_celsius(als_kelvin)
    # Voorkom gezeur over afrondfouten.
    als_celsius.waarde = pytest.approx(als_celsius.waarde)
    assert als_celsius == weer_als_celsius


def test_ongeldige_omzetting():
    """Je kan een temperatuur niet omzetten in een afstand."""

    temperatuur = kelvin(100)
    with pytest.raises(KanNietOmrekenen):
        meter(temperatuur)


def test_vergelijk_andere_eenheden():
    """Een temperatuur is niet hetzelfde als een afstand."""

    temperatuur = kelvin(100)
    afstand = meter(100)
    assert temperatuur != afstand
