"""test_keuze.py - Tests voor Keuze-objecten."""

import pyrpg


def test_antwoorden_zijn_uitspraken():
    """Antwoorden moeten tussen aanhalingstekens staan."""
    tekst_antwoord = "dit is het antwoord"
    antwoord = pyrpg.Antwoord(tekst_antwoord, None)
    assert antwoord.tekst_optie == '"{}"'.format(tekst_antwoord)
