"""test_wonen.py - Testfuncties voor wonen en huizen."""

import pytest

from pyrpg.gebouw import Ingang, InGebouw, Thuis
from pyrpg.gedrag import (
    ThuisNodig,
    bepaal_wat_te_doen,
    geef_doelstelling,
    noodzakelijk,
    zoek_pad,
)
from pyrpg.plek import Begroeiing, Begroeiingssoort, Plek, Richting, Verbinding
from pyrpg.stof import BestaatUit, Steen
from pyrpg.tijd import Afteller, uit_dagen, uit_uren
from zond import Zond


def test_huis_kiezen():
    """Als je geen huis hebt, kun je eentje uitkiezen."""
    zond = Zond()
    afteller = Afteller()
    speler = zond.zaak()
    plek = zond.zaak()
    zond.betrek(Plek, wat=speler, waar=plek)
    leeg_gebouw = zond.zaak()
    kamer = zond.zaak()
    zond.betrek(InGebouw, plek=kamer, gebouw=leeg_gebouw)
    zond.betrek(Ingang, omhulsel=leeg_gebouw, omhuld=kamer)
    Verbinding.betrek_symmetrisch(zond, plek, kamer, Richting.in_)

    geef_doelstelling(zond, speler, ThuisNodig, prioriteit=noodzakelijk)

    assert bepaal_wat_te_doen(zond, afteller, speler)

    afteller.tijd_vooruit(uit_uren(1))

    assert zond.geef(Thuis.bewoner, speler).gebouw == leeg_gebouw


def test_huis_bouwen():
    """Als je geen huis hebt, kun je eentje bouwen."""
    zond = Zond()
    afteller = Afteller()
    speler = zond.zaak()
    plek = zond.zaak()
    zond.betrek(Plek, wat=speler, waar=plek)
    zond.betrek(Begroeiing, plek=plek, begroeiing=Begroeiingssoort.bomen)
    bouwmateriaal = zond.zaak()
    rotsiet = zond.zaak()
    zond.betrek(Steen, stof=rotsiet)
    zond.betrek(BestaatUit, voorwerp=bouwmateriaal, stof=rotsiet)
    zond.betrek(Plek, wat=bouwmateriaal, waar=plek)

    geef_doelstelling(zond, speler, ThuisNodig, prioriteit=noodzakelijk)

    assert bepaal_wat_te_doen(zond, afteller, speler)

    afteller.tijd_vooruit(uit_uren(1))

    assert zond.geef(Thuis.bewoner, speler)
