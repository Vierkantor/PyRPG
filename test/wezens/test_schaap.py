"""wezens/test_schaap.py - Tests voor schapen en hun genetica."""
from pyrpg import Wezen
from pyrpg.kruising import BoolseEigenschap
from pyrpg.wezens.schaap import (
    beschrijf_vacht,
    geef_vachtkleur,
    geen_vlekken,
    heeft_zichtbare_vlekken,
    maak_schaap,
    wit,
    zwart_ipv_bruin,
)
from zond import Zond

helemaal = BoolseEigenschap(True, True)
half = BoolseEigenschap(True, False)
niet = BoolseEigenschap(False, False)


def test_beschrijving_wit_schaap():
    """Witte schapen hebben allemaal dezelfde vachtbeschrijving."""
    zond = Zond()
    eigenschappen = {wit: helemaal}
    schaap = maak_schaap(zond, eigenschappen=eigenschappen)
    assert (
        beschrijf_vacht(zond.geef(Wezen.wezen, schaap)) == "De vacht is helemaal wit."
    )


def test_vachtkleur_wit_schaap():
    """Het witte gen zorgt voor witte vachtkleur."""
    zond = Zond()

    eigenschappen = {wit: helemaal}
    schaap = maak_schaap(zond, eigenschappen=eigenschappen)
    assert geef_vachtkleur(zond.geef(Wezen.wezen, schaap)) == "wit"

    eigenschappen = {wit: half}
    schaap = maak_schaap(zond, eigenschappen=eigenschappen)
    assert geef_vachtkleur(zond.geef(Wezen.wezen, schaap)) == "wit"


def test_vachtkleur_zwart_schaap():
    """Het niet-witte en wel-zwarte gen zorgt voor zwarte vachtkleur."""
    zond = Zond()

    eigenschappen = {wit: niet, zwart_ipv_bruin: helemaal}
    schaap = maak_schaap(zond, eigenschappen=eigenschappen)
    assert geef_vachtkleur(zond.geef(Wezen.wezen, schaap)) == "zwart"


def test_vachtkleur_bruin_schaap():
    """Het niet-witte en niet-zwarte gen zorgt voor bruine vachtkleur."""
    zond = Zond()

    eigenschappen = {wit: niet, zwart_ipv_bruin: niet}
    schaap = maak_schaap(zond, eigenschappen=eigenschappen)
    assert geef_vachtkleur(zond.geef(Wezen.wezen, schaap)) == "bruin"


def test_wit_domineert_vachtkleur():
    """Als je een wit gen hebt, dan ben je helemaal wit."""
    zond = Zond()

    eigenschappen = {wit: helemaal, zwart_ipv_bruin: niet}
    schaap = maak_schaap(zond, eigenschappen=eigenschappen)
    assert geef_vachtkleur(zond.geef(Wezen.wezen, schaap)) == "wit"

    eigenschappen = {wit: helemaal, zwart_ipv_bruin: helemaal}
    schaap = maak_schaap(zond, eigenschappen=eigenschappen)
    assert geef_vachtkleur(zond.geef(Wezen.wezen, schaap)) == "wit"


def test_beschrijving_gevlekt_schaap():
    """Als een schaap niet wit is en wel vlekken heeft, dan staat dat in de beschrijving."""
    zond = Zond()

    eigenschappen = {wit: niet, geen_vlekken: niet}
    schaap = maak_schaap(zond, eigenschappen=eigenschappen)
    assert "vlek" in beschrijf_vacht(zond.geef(Wezen.wezen, schaap))


def test_vlekken_zijn_zichtbaar():
    """Als een schaap niet wit is en wel vlekken heeft, dan zijn de vlekken zichtbaar."""
    zond = Zond()

    eigenschappen = {wit: niet, geen_vlekken: niet}
    schaap = maak_schaap(zond, eigenschappen=eigenschappen)
    assert heeft_zichtbare_vlekken(zond.geef(Wezen.wezen, schaap))


def test_witte_vlekken_zijn_onzichtbaar():
    """Als een schaap wel wit is, dan zijn de vlekken niet zichtbaar."""
    zond = Zond()

    eigenschappen = {wit: helemaal, geen_vlekken: niet}
    schaap = maak_schaap(zond, eigenschappen=eigenschappen)
    assert not heeft_zichtbare_vlekken(zond.geef(Wezen.wezen, schaap))

    eigenschappen = {wit: half, geen_vlekken: niet}
    schaap = maak_schaap(zond, eigenschappen=eigenschappen)
    assert not heeft_zichtbare_vlekken(zond.geef(Wezen.wezen, schaap))
