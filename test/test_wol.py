"""test_wol.py - Testfuncties voor dingen die te maken hebben met wol."""

import pytest

from pyrpg.gedrag import OnhaalbaarError, bepaal_wat_te_doen, geef_doelstelling, noodzakelijk, voorwerpen_voorhanden_met
from pyrpg.plek import Plek
from pyrpg.tijd import Afteller, uit_uren
from pyrpg.wezens.schaap import maak_schaap
from pyrpg.wol.gedrag import GereinigdeWolVoorhanden, ScheerwolVoorhanden, WolgarenVoorhanden
from pyrpg.wol.goed import GereinigdeWol, Scheerwol, Wolgaren
from zond import Zond


def test_wolgaren_voorhanden_zonder_schaap():
    """Je kan geen wolgaren bekomen zonder een schaap in de buurt."""
    zond = Zond()
    afteller = Afteller()
    plek = zond.zaak()
    speler = zond.zaak()
    zond.betrek(Plek, wat=speler, waar=plek)

    geef_doelstelling(zond, speler, WolgarenVoorhanden, prioriteit=noodzakelijk)

    with pytest.raises(OnhaalbaarError) as exc_info:
         bepaal_wat_te_doen(zond, afteller, speler)


def test_scheerwol_voorhanden():
    """Doorloop het productieproces om scheerwol te bekomen."""
    zond = Zond()
    afteller = Afteller()
    plek = zond.zaak()
    speler = zond.zaak()
    zond.betrek(Plek, wat=speler, waar=plek)
    schaap = maak_schaap(zond)
    zond.betrek(Plek, wat=schaap, waar=plek)

    geef_doelstelling(zond, speler, ScheerwolVoorhanden, prioriteit=noodzakelijk)

    assert bepaal_wat_te_doen(zond, afteller, speler)

    afteller.tijd_vooruit(uit_uren(1))

    assert zond.alles(Scheerwol)
    assert voorwerpen_voorhanden_met(zond, speler, lambda zond, wat: bool(list(zond.zeef(Scheerwol.goed, wat)))
)


def test_gereinigde_wol_voorhanden():
    """Doorloop het productieproces om gereinigde wol te bekomen."""
    zond = Zond()
    afteller = Afteller()
    plek = zond.zaak()
    speler = zond.zaak()
    zond.betrek(Plek, wat=speler, waar=plek)
    schaap = maak_schaap(zond)
    zond.betrek(Plek, wat=schaap, waar=plek)

    geef_doelstelling(zond, speler, GereinigdeWolVoorhanden, prioriteit=noodzakelijk)

    assert bepaal_wat_te_doen(zond, afteller, speler)

    afteller.tijd_vooruit(uit_uren(1))

    print(list(voorwerpen_voorhanden_met(zond, speler, lambda zond, wat: bool(list(zond.zeef(Scheerwol.goed, wat))))))
    assert zond.alles(GereinigdeWol)


def test_wolgaren_voorhanden():
    """Doorloop het productieproces om wolgaren te bekomen."""
    zond = Zond()
    afteller = Afteller()
    plek = zond.zaak()
    speler = zond.zaak()
    zond.betrek(Plek, wat=speler, waar=plek)
    schaap = maak_schaap(zond)
    zond.betrek(Plek, wat=schaap, waar=plek)

    geef_doelstelling(zond, speler, WolgarenVoorhanden, prioriteit=noodzakelijk)

    assert bepaal_wat_te_doen(zond, afteller, speler)

    afteller.tijd_vooruit(uit_uren(10))

    assert zond.alles(Wolgaren)
