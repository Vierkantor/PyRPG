"""test_tijd.py - Tests voor de tijdsmodule."""


from pyrpg.tijd import Afteller


def test_volgorde_gebeurtenissen():
    """Als er meerdere gebeurtenissen gepland zijn, gebeuren ze in de juiste volgorde."""
    afteller = Afteller()
    log = []

    def _gebeurtenis_a():
        log.append("a")

    def _gebeurtenis_b():
        log.append("b")

    # Laat 'a' voor 'b' gebeuren maar stel ze in omgekeerde volgorde in.
    afteller.stel_gebeurtenis_in(2, _gebeurtenis_b)
    afteller.stel_gebeurtenis_in(1, _gebeurtenis_a)

    # Nu moet eerst 'a' gebeuren, dan 'b'.
    afteller.tijd_vooruit(2)
    assert log == ["a", "b"]


def test_tijd_tweemaal_vooruit():
    """Als de tijd in twee stappen vooruit gaat, gebeurt nog steeds alles."""
    afteller = Afteller()
    log = []

    def _gebeurtenis_a():
        log.append("a")

    def _gebeurtenis_b():
        log.append("b")

    afteller.stel_gebeurtenis_in(1, _gebeurtenis_a)
    afteller.stel_gebeurtenis_in(2, _gebeurtenis_b)

    # Nu moet 'a' al zijn gebeurd en 'b' nog niet.
    afteller.tijd_vooruit(1)
    assert log == ["a"]
    # Nu moet 'b' ook gebeuren.
    afteller.tijd_vooruit(2)
    assert log == ["a", "b"]


def test_gebeurtenis_tegelijkertijd():
    """Twee gelijktijdige _gebeurtenissen gebeuren allebei, in volgorde van definitie."""
    afteller = Afteller()
    log = []

    def _gebeurtenis_a():
        log.append("a")

    def _gebeurtenis_b():
        log.append("b")

    afteller.stel_gebeurtenis_in(1, _gebeurtenis_a)
    afteller.stel_gebeurtenis_in(1, _gebeurtenis_b)
    afteller.tijd_vooruit(1)

    # We eisen dat de volgorde overeenkomt.
    assert log == ["a", "b"]
