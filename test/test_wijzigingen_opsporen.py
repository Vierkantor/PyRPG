"""test_wijzigingen_opsporen.py - Controleer dat wijzigingen opsporen werkt als verwacht."""

import os.path

import pyrpg


def test_pyrpg_geimporteerd():
    """pyrpg staat tussen de geimporteerde modules"""
    modules = set(pyrpg.geimporteerde_modules())
    assert pyrpg in modules


def test_bestand_pyrpg_geimporteerd():
    """pyrpg.py staat tussen de geimporteerde bestanden"""
    bestanden = set(pyrpg.geimporteerde_bestanden())
    assert os.path.abspath("pyrpg/__init__.py") in bestanden
