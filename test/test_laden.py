"""test_laden.py - Test dat het laden van de wereld werkt."""

import importlib

import pyrpg
from zond import Zond


def test_herladen_pyrpg():
    """De module pyrpg zelf herladen moet lukken."""
    importlib.reload(pyrpg)


def test_laden_wereld():
    """Het moet lukken om een wereld te krijgen."""
    zond = Zond()
    pyrpg.maak_wereld(zond)
