"""test_opslaan.py - Test dat het opslaan en opnieuw laden van de wereld werkt."""

from pyrpg import maak_wereld
from pyrpg.opslaan import laad_in, opslagmap, sla_op
from zond import Zond


def test_opslaan_en_laden_maak_wereld():
    """Een vers gemaakte wereld moet opslaanbaar en inlaadbaar zijn."""
    zond = Zond()
    maak_wereld(zond)
    opgeslagen = sla_op(zond)
    ingeladen = laad_in(opgeslagen)
    # TEDOEN: betere vergelijking verzinnen?
    assert len(ingeladen.betrekking_per_kolom) == len(zond.betrekking_per_kolom)


def test_alle_werelden_laden():
    """Probeer elke wereld die is meegeleverd te laden."""
    for wereld_pad in opslagmap.iterdir():
        with wereld_pad.open() as wereld_bestand:
            laad_in(wereld_bestand.read())
