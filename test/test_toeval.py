"""test_toeval.py - Tests voor de toevalsgeneratie."""

from pyrpg.toeval import d


def test_dn_is_1dn():
    """De uitkomsten voor d(n) liggen in [1, n]."""
    n = 10
    assert d(n) in range(1, n + 1)


def test_interval_kdn():
    """De uitkomsten voor d(k, n) liggen in [k, k * n]."""
    k = 4
    n = 6
    assert d(k, n) in range(k, k * n + 1)
