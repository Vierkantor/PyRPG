"""test_toevalsmagie.py - tests voor de toevalsmagie-module"""


import pytest

from pyrpg import Omgeving, Plek, maak_personage
from pyrpg.effect import Effect
from pyrpg.gebouw import Ingang, Thuis
from pyrpg.lichaamsbouw.mens import bouw_menselijk_lichaam
from pyrpg.plek import Begroeiing, Begroeiingssoort, Richting, Verbinding
from pyrpg.poel import Poel
from pyrpg.stof import BestaatUit, Steen
from pyrpg.tijd import Afteller, uit_dagen
from pyrpg.toevalsmagie import (
    ProbeerOpnieuw,
    _gevolgen,
    begroeiing_kaal_rond_huis_onderwerp,
    eendjes_volgen_onderwerp,
    geef_toevallig_gevolg,
    kelder_in_huis_onderwerp,
    omgeving_onderwerp_zinkt_weg,
    onderwerp_krijgt_olijfolie_uit_oren,
    onderwerps_vingers_verstenen,
    pas_toevallig_gevolg_toe,
    zwerm_bijen_rond_onderwerp,
)
from zond import Zond


def test_toevallig_gevolg_geven():
    """geef_toevallig_gevolg geeft een functie die we kunnen aanroepen."""
    zond = Zond()
    afteller = Afteller()
    speler = maak_personage(zond, "de speler")
    doelwit = maak_personage(zond, "het doelwit")

    gevolg = geef_toevallig_gevolg()

    # Het gevolg kan irrelevant zijn, dat is prima.
    try:
        gevolg(zond, afteller, speler, doelwit)
    except ProbeerOpnieuw:
        pass


def test_toevallig_gevolg_toepassen():
    """pas_toevallig_gevolg_toe gooit geen ProbeerOpnieuw."""
    zond = Zond()
    afteller = Afteller()
    speler = maak_personage(zond, "de speler")
    doelwit = maak_personage(zond, "het doelwit")

    pas_toevallig_gevolg_toe(zond, afteller, speler, doelwit)


@pytest.mark.parametrize("gevolg", _gevolgen)
def test_alle_gevolgen_op_onderwerp(gevolg):
    """Pas alle gevolgen een voor een toe op een onderwerp."""
    zond = Zond()
    afteller = Afteller()
    speler = maak_personage(zond, "de speler")

    # Het gevolg kan irrelevant zijn, dat is prima.
    try:
        gevolg(zond, afteller, speler, None)
    except ProbeerOpnieuw:
        pass


@pytest.mark.parametrize("gevolg", _gevolgen)
def test_alle_gevolgen_op_voorwerp(gevolg):
    """Pas alle gevolgen een voor een toe op een onderwerp en een voorwerp."""
    zond = Zond()
    afteller = Afteller()
    speler = maak_personage(zond, "de speler")
    doelwit = maak_personage(zond, "het doelwit")

    # Het gevolg kan irrelevant zijn, dat is prima.
    try:
        gevolg(zond, afteller, speler, doelwit)
    except ProbeerOpnieuw:
        pass


def test_versteende_vingers():
    """Versteende vingers zijn gemaakt van een bestaande steensoort.

    Als er maar een steensoort is, zijn ze dus daarvan gemaakt.
    """
    zond = Zond()
    afteller = Afteller()
    speler = maak_personage(zond, "de speler")
    bouw_menselijk_lichaam(zond, speler)
    # De unieke steensoort.
    rotsiet = zond.zaak()
    zond.betrek(Steen, stof=rotsiet)

    vingers = onderwerps_vingers_verstenen(zond, afteller, speler, None)

    for vinger in vingers:
        materialen = list(zond.zoek(BestaatUit.voorwerp, vinger.deel))
        assert len(materialen) == 1
        assert materialen[0].stof == rotsiet


def test_bijen_verdwijnen():
    """Als een personage bijen om zich heen krijgt, verdwijnen die na verloop van tijd."""
    zond = Zond()
    afteller = Afteller()
    afteller = Afteller()
    speler = maak_personage(zond, "de speler")
    wachttijd = uit_dagen(50)

    # Eerst moet de speler een zwerm bijen krijgen...
    effect = zwerm_bijen_rond_onderwerp(zond, afteller, speler, None)
    assert effect in zond.zoek(Effect.onderwerp, speler)

    # Dan wachten we een flinke tijd, tot de effecten over zijn.
    afteller.tijd_vooruit(wachttijd)
    assert effect not in zond.zoek(Effect.onderwerp, speler)


def test_eendjes_volgen_onderwerp():
    """Als eendjes het onderwerp volgen, zijn die in het begin op dezelfde plek."""
    zond = Zond()
    afteller = Afteller()
    speler = maak_personage(zond, "de speler")
    lege_plek = zond.zaak()
    zond.betrek(Plek, wat=speler, waar=lege_plek)

    eendjes = eendjes_volgen_onderwerp(zond, afteller, speler, None)

    aanwezigen = set(plek.wat for plek in zond.zoek(Plek.waar, lege_plek))
    for eendje in eendjes:
        assert eendje in aanwezigen


def test_olijfolie_uit_oren():
    """Als het onderwerp olijfolie uit hun oren krijgt, ligt er een poel op de grond."""
    zond = Zond()
    afteller = Afteller()
    speler = maak_personage(zond, "de speler")
    lege_plek = zond.zaak()
    zond.betrek(Plek, wat=speler, waar=lege_plek)

    onderwerp_krijgt_olijfolie_uit_oren(zond, afteller, speler, None)

    assert zond.zoek(Poel.plek, lege_plek)


def test_begroeiing_kaal_rond_huis_onderwerp_begroeid():
    """Een onderwerp met een huis in een beboste omgeving krijgt opeens een kale omgeving."""
    zond = Zond()
    afteller = Afteller()
    speler = maak_personage(zond, "de speler")
    huis = zond.zaak()
    zond.betrek(Thuis, bewoner=speler, gebouw=huis)
    bosje = zond.zaak()
    zond.betrek(Omgeving, buitenom=bosje, binnenin=huis)
    zond.betrek(Begroeiing, plek=bosje, begroeiing=Begroeiingssoort.bomen)

    begroeiing_kaal_rond_huis_onderwerp(zond, afteller, speler, None)

    nieuwe_begroeiing = zond.geef(Begroeiing.plek, bosje)
    assert nieuwe_begroeiing.begroeiing == Begroeiingssoort.kaal


def test_begroeiing_kaal_rond_huis_onderwerp_ongedefinieerd():
    """Een onderwerp met een huis in een omgeving krijgt opeens een kale omgeving."""
    zond = Zond()
    afteller = Afteller()
    speler = maak_personage(zond, "de speler")
    huis = zond.zaak()
    zond.betrek(Thuis, bewoner=speler, gebouw=huis)
    generieke_omgeving = zond.zaak()
    zond.betrek(Omgeving, buitenom=generieke_omgeving, binnenin=huis)

    begroeiing_kaal_rond_huis_onderwerp(zond, afteller, speler, None)

    nieuwe_begroeiing = zond.geef(Begroeiing.plek, generieke_omgeving)
    assert nieuwe_begroeiing.begroeiing == Begroeiingssoort.kaal


def test_omgeving_onderwerp_zinkt_weg():
    """Waar het onderwerp stond is nu een put in de grond."""
    zond = Zond()
    afteller = Afteller()
    speler = maak_personage(zond, "de speler")
    terrein = zond.zaak()
    zond.betrek(Plek, wat=speler, waar=terrein)

    put = omgeving_onderwerp_zinkt_weg(zond, afteller, speler, None)

    omhoog = zond.zeef(Verbinding.van, put).zeef(Verbinding.naar, terrein).geef()
    assert omhoog[0].richting == Richting.op


def test_kelder_in_huis_onderwerp():
    """Vanuit de nieuwe kelder kun je weer omhoog naar het huis."""
    zond = Zond()
    afteller = Afteller()
    speler = maak_personage(zond, "de speler")
    huis = zond.zaak()
    zond.betrek(Thuis, bewoner=speler, gebouw=huis)
    hal = zond.zaak()
    zond.betrek(Ingang, omhulsel=huis, omhuld=hal)

    kelder = kelder_in_huis_onderwerp(zond, afteller, speler, None)

    omhoog = zond.zeef(Verbinding.van, kelder).zeef(Verbinding.naar, hal).geef()
    assert omhoog[0].richting == Richting.op
