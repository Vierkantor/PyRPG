"""test_kruising.py - Testfuncties voor het kruisen van wezens."""

import pytest

from pyrpg.kruising import KruisingInfo, KruisingSoort
from pyrpg.wezen import KanNietKruisen, Wezen, kruis
from pyrpg.wezens.eend import maak_eend
from pyrpg.wezens.schaap import maak_schaap
from zond import Zond


def test_kruisen_schapen_lukt():
    """Je kan altijd twee schapen met elkaar kruisen."""
    schaap_info1 = KruisingInfo(KruisingSoort.schaap, {})
    schaap_info2 = KruisingInfo(KruisingSoort.schaap, {})
    assert KruisingInfo.kruising_mogelijk(schaap_info1, schaap_info2)


def test_kruisen_kraaien_en_eenden_lukt_niet():
    """Je kan niet zomaar een eend en een kraai kruisen."""
    eend_info = KruisingInfo(KruisingSoort.eend, {})
    kraai_info = KruisingInfo(KruisingSoort.kraai, {})
    assert not KruisingInfo.kruising_mogelijk(eend_info, kraai_info)


def test_kruisen_schapen_geeft_schaap():
    """Als je de kruising neemt van twee schapen, krijg je een schaap."""
    zond = Zond()
    pietje = maak_schaap(zond, "pietje")
    henkie = maak_schaap(zond, "henkie")
    bertus = kruis(
        zond, zond.geef(Wezen.wezen, pietje), zond.geef(Wezen.wezen, henkie), "bertus"
    )
    assert zond.geef(Wezen.wezen, bertus).kruisinginfo.soort == KruisingSoort.schaap


def test_kruisen_eend_en_schaap_geeft_fout():
    """Als je een eend en een schaap wilt kruisen, krijg je een exceptie."""
    zond = Zond()
    een_plek = zond.zaak()
    schaap = maak_schaap(zond)
    eend = maak_eend(zond, een_plek)
    with pytest.raises(KanNietKruisen):
        kruis(
            zond,
            zond.geef(Wezen.wezen, schaap),
            zond.geef(Wezen.wezen, eend),
            "een heel vreemd wezen",
        )
