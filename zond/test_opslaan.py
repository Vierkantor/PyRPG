"""test_opslaan.py - Testen van opslaan en laden in Zond."""

import pytest
from camel import Camel, CamelRegistry
from hypothesis import given
from hypothesis import strategies as st

from zond import Betrekking, Zaak, Zond, zond_naar_yaml


def test_herladen_opsommen():
    """Als we een betrekking hebben met een zaak erin, en we herladen Zond, dan krijgen we die terug."""
    zond = Zond()
    zaak = zond.zaak()

    class EenBetrekking(Betrekking):
        een_zaak = Zaak
        een_getal = int

    een_betrekking_naar_yaml = CamelRegistry()

    @een_betrekking_naar_yaml.dumper(EenBetrekking, "EenBetrekking", version=1)
    def _dump_een_betrekking(een_betrekking):
        return {
            "een_zaak": een_betrekking.een_zaak,
            "een_getal": een_betrekking.een_getal,
        }

    @een_betrekking_naar_yaml.loader("EenBetrekking", version=1)
    def _laad_een_betrekking(een_betrekking, version=1):
        return EenBetrekking(
            een_zaak=een_betrekking["een_zaak"], een_getal=een_betrekking["een_getal"]
        )

    zond.betrek(EenBetrekking, een_zaak=zaak, een_getal=37)

    camel = Camel([zond_naar_yaml, een_betrekking_naar_yaml])
    opgeslagen = camel.dump(zond)
    ingeladen = camel.load(opgeslagen)

    opsomming = list(ingeladen.opsomming(EenBetrekking))
    assert len(opsomming) == 1
    een_betrekking = opsomming[0]
    assert een_betrekking.een_zaak.zond == ingeladen
    assert een_betrekking.een_getal == 37
