"""test_verzinnen.py - Testen van nieuwe betrekkingen verzinnen."""

import pytest
from camel import Camel, CamelRegistry
from hypothesis import given
from hypothesis import strategies as st

from zond import Betrekking, Zaak, Zond


def test_leeg_verzinnen():
    """Bij een lege Zond geeft de `geef_of_verzin'-methode de uitkomst van verzinnen."""
    zond = Zond()
    zaak = zond.zaak()

    class EenBetrekking(Betrekking):
        een_zaak = Zaak
        een_getal = int

        @classmethod
        def verzin(cls, zond, kolommen):
            return zond.betrek(
                cls,
                een_zaak=kolommen.get("een_zaak", zond.zaak()),
                een_getal=kolommen.get("een_getal", 37),
            )

    betrekking = zond.geef_of_verzin(EenBetrekking.een_zaak, zaak)
    assert isinstance(betrekking, EenBetrekking)
    assert betrekking.een_zaak == zaak
    assert betrekking.een_getal == 37
