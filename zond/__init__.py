"""Zond - een Entiteit-Component-Systeemraamwerk voor Python.

Waarom de naam?
===============
Het woord `entiteit' komt uit Latijnse bestanddelen `ent-' en `-tas',
waarvan het eerste deel heeft geleid tot het Nederlandse woord `zonde' (overtreding).
Samen met het tweede deel geeft dit een evenknie die een vorm heeft in de buurt van `zòndede'.
Omdat dat er niet zo heel geweldig uitziet, maken we er `zond' van.

Bovendien heeft `zond' nog wel wat meer mooie betekenissen die bij ons doel passen:
de bekendste zal wel de verledentijdsstam zijn van `zenden',
wat een verwijzing kan zijn naar de berichten die onze componenten zullen versturen.
maar er zijn ook vergeten woorden (in de zin van https://taaldacht.nl/vergeten-woorden)
te vinden die mooi samenhangen.
Denk hierbij het `zond' dat we nog kennen als onderdeel van `gezond',
met oorspronkelijke betekenis kracht, gezondheid.
Met Zond wordt je programma weer gezond!
Verder is `zonder' een nevenvorm van `zwander', wat een (voortgedreven) kudde dieren betekent,
wat dan beelden moet oproepen van het Systeem dat de componenten voortdrijft.

Ten slotte is `zond' de Russische vorm van `sonde', en als zodanig gebruikt voor
een serie ruimtesondes die de diepe ruimte ingingen. Dat is toch ook een mooie naam!

En wat is daar het praktisch nut van?
=====================================
Het grote nut van entiteit-component-systeemraamwerken is dat je minder zorgen
hoeft te maken hoe data wordt opgeslagen en hoe het gekoppeld wordt met code.
In die zin is het net als objectraamwerken.
Zoals een relationele databaas staat tegenover de datavork in oude Mac-bestanden,
zo ongeveer staat entitieit-component-systeem tegenover object-georiënteerd.
(En zoals relationele databazen staan tegenover platte tekst,
zo staat entititeit-component-systeem tegeneover alles in machinecode schrijven.

We nemen alleen niet de doorsneenaamgeving over:
een `entiteit' wordt een `Zaak'
    (het is niet echt een `ding' en ook niet echt een `eenheid'),
een `component' wordt een `Betrekking'
    (want het kan `rol' betekenen, maar je kan met Zond ook
    verbanden tussen meerdere zaken vastleggen in een betrekking),
en een `systeem' wordt een `Stelsel'
    TEDOEN: deze naam kan beter.

Zie de Zond-klasse voor verdere gebruiksinformatie.

Huh, verbanden tussen meerdere zaken?
=====================================
Als een Persoon een thuis heeft, dan kun je vanuit dat thuis
wel eens willen weten wie daar nog meer woont.
Vandaar dat de verbanden net zo belangrijk worden als `platte' data.
"""
from collections import defaultdict

from camel import CamelRegistry


class ZondVerwarringError(Exception):
    """Wordt gegooid als je een Zaak hebt in een Zond-object dat je aan een ander Zond-object wilt voeren."""

    def __init__(self, verwarde_zond, zaak):
        """Constructor.

        :param verwarde_zond: Het Zond-object dat niet hoort bij de zaak.
        :param zaak: De Zaak die aan de verwarde_zond is gevoerd.
        """
        self.verwarde_zond = verwarde_zond
        self.zaak = zaak


class NietUniekError(Exception):
    """Wordt gegooid als je zegt dat het gevraagde uniek is, maar dat niet zo is."""

    def __init__(self, kolom, zaak, aantal):
        """Constructor.

        :param kolom: De kolom waarop gezocht is.
        :param zaak: De zaak waarop gezocht is.
        :param aantal: Het aantal gevonden betrekkingen.
        """
        assert aantal != 1

        self.kolom = kolom
        self.zaak = zaak
        self.aantal = aantal


class BestaatNietError(NietUniekError):
    """Het gevraagde is niet uniek omdat het helemaal niet bestaat."""


class ReedsBetrokkenError(Exception):
    """Wordt gegooid als een Betrekking al te vinden is in een Uitkomst,
    en nogmaals eraan wordt gebonden."""

    def __init__(self, uitkomst, kolom):
        """Constructor.

        :param uitkomst: De Uitkomst waaraan is gepoogd de betrekking te binden.
        :param kolom: De kolom die gebonden zou moeten worden.
        """
        self.uitkomst = uitkomst
        self.kolom = kolom


class Zond:
    """Drager van informatie die alles aan elkaar lijmt.
    We geven deze objecten expliciet door om minder last te hebben van globale waarden.

    Een stoomcursus aanelkaarlijmen:
    we gaan vooralsnog ervan uit dat we een losse Zond gaan gebruiken, dus laten we die aanmaken:
        zond = Zond()
    vervolgens maken we onze eerste Zaak aan:
        de_zaak = zond.zaak()
    definieer een Betrekking genaamd EenBetrekking aan (zie Betrekking voor hoe je dat doet),
    en hang die aan de Zaak:
        zond.betrek(EenBetrekking, een_zaak=de_zaak, een_getal=37)

    We kunnen ook meerdere zaken betrekken:
        class Tweeplaatsig(Betrekking):
            vanuit = Zaak
            naartoe = Zaak
        andere_zaak = zond.zaak()
        zond.betrek(Tweeplaatsig, vanuit=de_zaak, naartoe=andere_zaak)

    Uiteraard kunnen we meerdere zaken samen met platte gegevens vermengen in een betrekking,
    en nog wat meer leuke trucjes.

    Dit werkt prima voor aanmaken van onze wereld, maar het is nog niet zo interactief.
    Ten eerste zou je willen kunnen opzoeken welke zaken een bepaalde betrekking hebben.
    Je kan ze los opsommen:
        for een_betrekking in zond.opsomming(EenBetrekking):
            print(een_betrekking.een_getal)
    maar je kan ook joins nemen (alle tupels betrekkingen zodanig dat de aangegeven velden dezelfde Zaak zijn):
        for een_betrekking, tweeplaatsige_betrekking, in zond.betrokken(EenBetrekking.een_zaak, Tweeplaatsig.vanuit):
            print(een_betrekking.een_getal)
    en tenslotte kun je gegeven een betrekking de zaak opzoeken in een andere betrekking:
        for een_betrekking in zond.opsomming(EenBetrekking):
            for tweeplaatsige_betrekking in zond.zoek(Tweeplaatsig.vanuit, een_betrekking.een_zaak):
                print(tweeplaatsige_betrekking.naartoe)
    (dit laatste is vooral handig als je nog niet hebt besloten welke betrekking je gaat joinen bij het opsommen,
    of als je meerdere joins wilt maken op verschillende zaken).
    Overigens ligt de volgorde van deze opsommingen niet vast.

    Een Zaak die je krijgt van de zaak-mehtode kun je niet aan een ander Zond-object voeren,
    dan krijg je een ZondVerwarringError.
    (Je kan niet zomaar dingen in een ander heelal stoppen!)
    """

    def __init__(self):
        self.vrij_id = 0

        # Stuur (kolom, zaak) naar de betrekkingen.
        self.betrekking_per_kolom = defaultdict(list)
        # Stuur cls naar de betrekkingen.
        self.betrekking_per_type = defaultdict(list)

    def geef_nieuw_id(self):
        """Geef een id voor een Zaak die nog niet gebruikt wordt."""
        resultaat = self.vrij_id
        self.vrij_id += 1
        return resultaat

    def zaak(self, omschrijving=None):
        """Maak een nieuwe Zaak aan."""
        return Zaak(self.geef_nieuw_id(), self, omschrijving=omschrijving)

    def betrek(self, cls, **kwargs):
        """Maak een nieuwe betrekking aan met gegeven velden.

        :type cls: MetaBetrekking
        :rtype: Betrekking
        """
        betrekking = cls(**kwargs)
        self.betrekking_per_type[cls].append(betrekking)
        for zaakkolom in cls.zaken:
            zaak = kwargs[zaakkolom]

            # De zaak moet wel horen bij onszelf.
            # Als de Zaak nog geen Zond heeft (bijvoorbeeld bij het inladen),
            # repareren we dat hier.
            if zaak.zond is None:
                zaak.zond = self
            elif zaak.zond != self:
                raise ZondVerwarringError(self, zaak)

            self.betrekking_per_kolom[cls.kolommen[zaakkolom], zaak].append(betrekking)

        return betrekking

    def onttrek(self, betrekking):
        """Haal de betrekking weg van alle zaken.

        Is redelijk duur in vergelijking met betrek.
        """
        cls = type(betrekking)
        self.betrekking_per_type[cls].remove(betrekking)
        for zaakkolom in cls.zaken:
            zaak = betrekking.geef(zaakkolom)
            self.betrekking_per_kolom[cls.kolommen[zaakkolom], zaak].remove(betrekking)

    def onttrek_alle(self, kolom, zaak):
        """Haal alle betrekkingen weg waar de zaak in de gegeven kolom staat."""
        # Giet het in een nieuwe lijst zodat we de betrekking niet onder onszelf vandaan weghalen.
        for betrekking in list(self.betrekking_per_kolom[kolom, zaak]):
            self.onttrek(betrekking)

    def vernietig(self, zaak):
        """Haal de zaak uit alle betrekkingen."""
        for ((kolom, mogelijke_zaak), betrekking) in list(
            self.betrekking_per_kolom.items()
        ):
            if mogelijke_zaak == zaak:
                try:
                    self.betrekking_per_type[kolom.cls].remove(betrekking)
                except ValueError:
                    # Het is al weggegooid, blijkbaar.
                    pass
                del self.betrekking_per_kolom[(kolom, zaak)]

    def _uitkomst_van_betrekkingen(self, cls, lijst):
        """Maak een Uitkomst van een lijst betrekkingen van de gegeven klasse."""
        return Uitkomst(self, ((betrekking,) for betrekking in lijst), {cls: 0})

    def alles(self, cls):
        """Geef een Uitkomst van alle betrekkingen van het gegeven type.

        :type cls: MetaBetrekking
        :rtype: Uitkomst
        """
        return self._uitkomst_van_betrekkingen(cls, self.betrekking_per_type[cls])

    def opsomming(self, cls):
        """Itereer over alle betrekkingen van het gegeven type.

        :type cls: MetaBetrekking
        """
        return self.alles(cls).alleen(cls)

    def betrokken(self, *args):
        """Itereer over alle tupels betrekkingen waarvoor de gegeven kolommen dezelfde zaak zijn.

        :param args: Een lijst van kolommen. Mag niet leeg zijn.

        :rtype: Uitkomst
        """

        van, *rest = args
        uitkomst = self.alles(van.cls)
        for naar in rest:
            uitkomst = uitkomst.bind(van, naar)
        return uitkomst

    def zeef(self, kolom, zaak):
        """Geef een Uitkomst van betrekkingen met gegeven zaak in die kolom.

        Wil je niet doorzoeken? Dan kun je ook `zoek' gebruiken.
        Wil je een unieke betrekking? Dan kun je ook `geef' gebruiken.

        :rtype: Uitkomst
        """
        return self._uitkomst_van_betrekkingen(
            kolom.cls, self.betrekking_per_kolom[kolom, zaak]
        )

    def zeef_zaken(self, kolom, zaken):
        """Geef een Uitkomst van betrekkingen met een van de gegeven zaken in die kolom.

        Als er maar een zaak is, kun je ook `zeef' gebruiken.

        :rtype: Uitkomst
        """
        uitkomst = []
        for zaak in zaken:
            uitkomst.extend(self.betrekking_per_kolom[kolom, zaak])
        return self._uitkomst_van_betrekkingen(kolom.cls, uitkomst)

    def zoek(self, kolom, zaak):
        """Itereer over de betrekkingen met de zaak in de gegeven kolom."""
        return self.zeef(kolom, zaak).alleen(kolom.cls)

    def geef(self, kolom, zaak):
        """Geef de unieke betrekking met de zaak in die kolom.

        :raise NietUniekError: Indien het toch niet uniek is, gooien we een NietUniekError.

        :param kolom: De kolom om op te zoeken.
        :type kolom: Kolom
        :param zaak: De zaak die in de kolom moet staan.
        :type zaak: Zaak
        :return: De unieke Betrekking met die waarde.
        :rtype: kolom.cls
        """
        return self.zeef(kolom, zaak).geef()[0]

    def geef_of_verzin(self, kolom, zaak):
        """Geef de unieke betrekking met de zaak in die kolom of verzin die.

        Indien er nog geen betrekking bestaat met de gegeven waarden,
        wordt een nieuwe betrekking aangemaakt in de zond en is dat de uitkomst.

        Dit vereist dat de betrekking van deze kolom een klassemethode `verzin' heeft,
        ongeveer van de volgende vorm:

            @classmethod
            def verzin(cls, zond, kolommen):
                return zond.betrek(cls, een_kolom=kolommen.get('een_kolom', zond.zaak()), ...)

        De `verzin'-methode moet dus zelf `betrek' aanroepen.

        :raise NietUniekError: Indien er (al) meerdere betrekkingen bestaan.

        :param kolom: De kolom om op te zoeken.
        :type kolom: Kolom
        :param zaak: De zaak die in de kolom moet staan.
        :type zaak: Zaak
        :return: De unieke Betrekking met die waarde.
        :rtype: kolom.cls
        """
        try:
            return self.geef(kolom, zaak)
        except BestaatNietError:
            return kolom.cls.verzin(self, {kolom.naam: zaak})

    def zoek_of_verzin(self, kolom, zaak):
        """De niet-lege lijst met betrekkingen met de zaak in die kolom, of eentje erbij verzonnen.

        Indien er nog geen betrekking bestaat met de gegeven waarden,
        wordt een nieuwe betrekking aangemaakt in de zond en is de uitkomst
        een lijst met precies die erin.

        Dit vereist dat de betrekking van deze kolom een klassemethode `verzin' heeft,
        ongeveer van de volgende vorm:

            @classmethod
            def verzin(cls, zond, kolommen):
                return zond.betrek(cls, een_kolom=kolommen.get('een_kolom', zond.zaak()), ...)

        De `verzin'-methode moet dus zelf `betrek' aanroepen.

        :raise NietUniekError: Indien er (al) meerdere betrekkingen bestaan.

        :param kolom: De kolom om op te zoeken.
        :type kolom: Kolom
        :param zaak: De zaak die in de kolom moet staan.
        :type zaak: Zaak
        :return: De unieke Betrekking met die waarde.
        :rtype: kolom.cls
        """
        gevonden = list(self.zoek(kolom, zaak))
        if gevonden:
            return gevonden
        else:
            return [kolom.cls.verzin(self, {kolom.naam: zaak})]

    def ontkever_zaak(self, gezochte_zaak):
        """Geef een lijst van alle betrekkingen met deze zaak.

        Erg inefficiënt, dus vooral bedoeld in ontkevering, niet voor actief gebruik.
        """
        for ((kolom, zaak), betrekking) in self.betrekking_per_kolom.items():
            if zaak == gezochte_zaak:
                yield betrekking


zond_naar_yaml = CamelRegistry()


@zond_naar_yaml.dumper(Zond, "zond", version=1)
def _dump_zond(zond):
    uitkomst = []
    for betrekkingen in zond.betrekking_per_type.values():
        uitkomst.extend(betrekkingen)
    return uitkomst


@zond_naar_yaml.loader("zond", version=1)
def _laad_zond(betrekkingen, version=1):
    zond = Zond()
    for betrekking in betrekkingen:
        zond.betrek(type(betrekking), **betrekking.__dict__)
    return zond


def hertupel(groepen, indices):
    """Itereer over hergeïndiceerde tupels samengesteld uit de groepen.

    Voor elk element g van groepen, geven we een nieuw tupel ht,
    met ht[i] = g[indices[i]].

    :param groepen: Iterable van tupels.
    :param indices: Iterable van indices voor de groepen.
    """
    for groep in groepen:
        yield tuple(groep[i] for i in indices)


class Uitkomst:
    """Wat je krijgt bij zoeken: een itereerbare hoeveelheid gegroepeerde betrekkingen.

    Het praktisch nut hiervan is dat we kunnen doorzoeken.
    Stel dat je bijvoorbeeld de volgende betrekkingen hebt:
        class Thuis(Betrekking):
            bewoner = Zaak
            gebouw = Zaak
        class Omgeving(Betrekking):
            binnenin = Zaak
            buitenom = Zaak
    En je wilt weten in welke omgeving het huis van een gegeven persoon is.
    Dan kun je doen:
        zond.zeef(Thuis.bewoner, persoon)\\  # geeft een Uitkomst
            .bind(Thuis.gebouw, Omgeving.binnenin)\\  # geeft een Uitkomst
            .geef()  # geeft een Betrekking (of gooit een NietUniekError)

    Bij itereren/geven komt alleen de laatste Betrekking eruit,
    maar bij binden kun je elke betrekking die al gebonden is, gebruiken.
    """

    def __init__(self, zond, betrekkinggroepen, betrekkingsvolgorde):
        self.zond = zond
        self.betrekkinggroepen = betrekkinggroepen
        self.betrekkingsvolgorde = betrekkingsvolgorde

    def _groepen_gebonden(self, van, naar):
        """Geef een iterator met de betrekkinggroepen als we binden."""
        van_index = self.betrekkingsvolgorde[van.cls]
        for betrekkinggroep in self.betrekkinggroepen:
            van_zaak = betrekkinggroep[van_index].geef(van.naam)
            for nieuwe_betrekking in self.zond.zoek(naar, van_zaak):
                yield (*betrekkinggroep, nieuwe_betrekking)

    def bind(self, van, naar):
        """Geef een nieuwe Uitkomst, verbonden op de kolommen van en naar.

        De nieuwe Uitkomst bestaat uit alle groepen betrekkingen
        die we kunnen maken uit de groepen in deze Uitkomst,
        samengevoegd met elke betrekking van de naar-kolom,
        waar de zaken van de van-kolom en naar-kolom overeenkomen.

        :raise KeyError: Indien de van-betrekking nog niet in deze uitkomst zit.
        :raise ReedsBetrokkenError: Indien de naar-betrekking al in deze uitkomst zit.
        :rtype: Uitkomst
        """
        if naar.cls in self.betrekkingsvolgorde:
            raise ReedsBetrokkenError(self, naar)
        return Uitkomst(
            self.zond,
            self._groepen_gebonden(van, naar),
            {**self.betrekkingsvolgorde, naar.cls: len(self.betrekkingsvolgorde)},
        )

    def zeef(self, kolom, waarde):
        """Geef een nieuwe Uitkomst met alleen de betrekkingen waar deze kolom deze waarde heeft.

        :raise KeyError: Indien de kolom niet hoort bij de betrekkingen in deze uitkomst.
        :rtype: Uitkomst
        """
        zeefbetrekking = self.betrekkingsvolgorde[kolom.cls]
        return Uitkomst(
            self.zond,
            [
                betrekkinggroep
                for betrekkinggroep in self.betrekkinggroepen
                if betrekkinggroep[zeefbetrekking].geef(kolom) == waarde
            ],
            self.betrekkingsvolgorde,
        )

    def beperk(self, *betrekkingen):
        """Beperk de betrekkingen in de groepen tot de gegeven betrekkingen.

        :raises KeyError: indien we beperken op een beperking die niet in de uitkomst staat.
        :rtype: Uitkomst
        """
        indices = [self.betrekkingsvolgorde[cls] for cls in betrekkingen]
        return Uitkomst(
            self.zond,
            hertupel(self.betrekkinggroepen, indices),
            {betrekking: i for i, betrekking in enumerate(betrekkingen)},
        )

    def alleen(self, betrekking):
        """Itereer over alleen de waarde van de gegeven betrekking, in plaats van een groep."""
        index = self.betrekkingsvolgorde[betrekking]
        yield from (groep[index] for groep in self.betrekkinggroepen)

    def geef(self):
        """Geef de unieke groep in deze uitkomst.

        :raise NietUniekError: Indien het toch niet uniek is, gooien we een NietUniekError.
        """

        # Verzamel de uitkomsten.
        self.betrekkinggroepen = list(self.betrekkinggroepen)

        if not self.betrekkinggroepen:
            raise BestaatNietError(
                self.betrekkingsvolgorde.keys(), None, len(self.betrekkinggroepen)
            )
        if len(self.betrekkinggroepen) != 1:
            assert len(self.betrekkinggroepen) > 1
            raise NietUniekError(
                self.betrekkingsvolgorde.keys(), None, len(self.betrekkinggroepen)
            )

        # Geef de eerste (enige) betrekkinggroep.
        return self.betrekkinggroepen[0]

    def waarden(self, kolom):
        """Itereer over de waarden die voorkomen in de gegeven kolom."""
        index = self.betrekkingsvolgorde[kolom.cls]
        yield from (groep[index].geef(kolom.naam) for groep in self.betrekkinggroepen)

    def __iter__(self):
        yield from self.betrekkinggroepen

    def __bool__(self):
        return bool(list(self.betrekkinggroepen))


class Zaak:
    """Het `aanraakbare' deel van het geheel.

    Een Zaak is niet meer dan een uniek `ding' waar betrekkingen aan hangen.
    (Vergelijk het met een constante in modeltheorie.)
    Uiteraard heeft het wat nuttige methoden voor de programmeur,
    maar in feite zou dit al een voldoende definitie zijn:

        Zond.zaak = object

    En vervolgens nemen we id(een_of_andere_zaak) als sleutelwaarde.
    Zolang je niet van plan bent om iets op te slaan, werkt dat prima.
    Dat zijn we wel van plan, dus we doen iets meer.

    Om aan een zaak te komen als gebruiker, volstaat de volgende aanroep:

        zaak = zond.zaak()

    De Zond-klasse zorgt ervoor dat de verscheidene invarianten behouden worden.
    """

    def __init__(self, id, zond, omschrijving=None):
        """Niet voor openbaar gebruik!"""
        self.id = id
        self.zond = zond
        self.omschrijving = None

    def __str__(self):
        if self.omschrijving:
            return f"Zaak({self.id}, {self.omschrijving})"
        else:
            return f"Zaak({self.id})"

    def __hash__(self):
        return self.id


@zond_naar_yaml.dumper(Zaak, "zaak", version=1)
def _dump_zaak(zaak):
    return (zaak.id,)


@zond_naar_yaml.loader("zaak", version=1)
def _laad_zaak(id, version=1):
    return Zaak(id[0], None)


class Kolom:
    """De naam van de velden in een betrekking.

    Deze is nuttig in de MetaBetrekking om bij te houden wat waar te vinden is.
    """

    def __init__(self, naam, type, cls):
        self.naam = naam
        self.type = type
        self.cls = cls

    def __str__(self):
        return f"{self.cls}.{self.naam} : {self.type}"

    def __repr__(self):
        return f"Kolom({self.naam}, {self.type}, {self.cls})"


class MetaBetrekking(type):
    """Een metaklasse om bij te houden welke waarden bij een betrekking horen."""

    def __init__(cls, name, bases, attrs):
        super().__init__(name, bases, attrs)

    def __new__(mcs, naam, ouders, clsdict):
        """Maak een nieuw soort Component aan."""

        # Kolommen worden gegeven als klassevariabelen van een Betrekking.
        # We moeten minstens een Zaak ertussen hebben zitten.
        kolommen = {}
        zaken = set()

        for name, attr in clsdict.items():
            if isinstance(attr, type):
                # De klasse kunnen we we er pas later instoppen.
                clsdict[name] = kolommen[name] = Kolom(name, attr, None)
                if attr == Zaak:
                    zaken.add(name)

        # TEDOEN: willen we ouderklassen eigenlijk wel toestaan?
        # Eigenlijk past het niet zo mooi in het hele ECS-ontwerp,
        # maar de code is prima te doen.
        for ouder in ouders:
            if isinstance(ouder, MetaBetrekking):
                # Zeur als de doorsnede niet leeg is
                if kolommen.keys() & ouder.kolommen.keys():
                    raise Exception("Herdefiniëren van componentvelden mag niet!")
                assert not (zaken & set(ouder.zaken)), (
                    "Kolommen hebben lege doorsnede"
                    + "en zaken zijn een deelverzameling, maar hun doorsnede is niet leeg :S"
                )

                # We moeten nieuwe kolommen maken voor subklassen,
                # want we gaan ervan uit dat een kolom uniek is per betrekking.
                for name, kolom in ouder.kolommen.items():
                    clsdict[name] = kolommen[name] = Kolom(kolom.naam, kolom.type, None)

                zaken.update(ouder.zaken)

        cls = super().__new__(mcs, naam, ouders, clsdict)

        for kolom in kolommen.values():
            kolom.cls = cls

        cls.kolommen = kolommen
        cls.zaken = tuple(zaken)  # Let op: we leggen hier een volgorde vast!

        return cls


class Betrekking(metaclass=MetaBetrekking):
    """Een betrekking bevat data die bij een zaak (of meer) hoort.

    Dit kun je het beste vergelijken met een rij uit een databaastabel.
    (En het zou me niets verbazen als men dit daadwerkelijk zo opslaat!)

    Voorbeeld:
        class EenBetrekking(Betrekking):
            een_zaak = Zaak
            een_getal = int

    Elke Betrekking moet minstens een Zaak tussen haar velden hebben.
    """

    def __init__(self, **waarden):
        """Maak een betrekking aan met de gegeven waarden.

        Je wilt waarschijnlijk Zond.betrek gebruiken.
        """
        self.__dict__ = waarden

    def geef(self, kolom):
        """Geef de waarde van het veld met de gegeven naam.

        Het is netter om de attributen te gebruiken, tenzij je dynamische trucjes uithaalt.

        :param kolom: een `Kolom'-object of een `str'
        :raise KeyError: als de kolom niet bestaat.
        """
        if isinstance(kolom, Kolom):
            kolomnaam = kolom.naam
        else:
            kolomnaam = kolom
        return self.__dict__[kolomnaam]
