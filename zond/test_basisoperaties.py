"""test_basisoperaties.py - Voer tests uit voor de basisoperaties in Zond."""

import pytest
from hypothesis import given
from hypothesis import strategies as st

from zond import (
    Betrekking,
    NietUniekError,
    ReedsBetrokkenError,
    Zaak,
    Zond,
    ZondVerwarringError,
    hertupel,
)


@st.composite
def succesvolle_hertupeling(draw, elements=st.integers(), max_lengte=10):
    lengte = draw(st.integers(min_value=1, max_value=max_lengte))
    groepen = draw(st.iterables(st.lists(elements, min_size=lengte, max_size=lengte)))
    indices = draw(st.lists(st.integers(min_value=0, max_value=lengte - 1)))
    return groepen, indices


@given(succesvolle_hertupeling())
def test_hertupel_succesvol(groepen_en_indices):
    groepen, indices = groepen_en_indices
    groepen = list(groepen)  # Leg de volgorde vast.

    hertupeld = hertupel(groepen, indices)

    for origineel, nieuw in zip(groepen, hertupeld):
        for i, ht in enumerate(nieuw):
            assert ht == origineel[indices[i]]


def test_betrekken():
    """De betrek-methode geeft een betrekking."""
    zond = Zond()
    zaak = zond.zaak()

    class EenBetrekking(Betrekking):
        een_zaak = Zaak
        een_getal = int

    betrekking = zond.betrek(EenBetrekking, een_zaak=zaak, een_getal=37)
    assert isinstance(betrekking, EenBetrekking)
    assert betrekking.een_zaak == zaak
    assert betrekking.een_getal == 37


def test_betrekking_geven():
    """Betrekkingen hebben een geef-methode die hetzelfde doet als attributen opvragen."""
    zond = Zond()
    zaak = zond.zaak()

    class EenBetrekking(Betrekking):
        een_zaak = Zaak
        een_getal = int

    betrekking = zond.betrek(EenBetrekking, een_zaak=zaak, een_getal=37)
    assert betrekking.geef("een_zaak") == zaak
    assert betrekking.geef("een_getal") == 37
    with pytest.raises(KeyError):
        betrekking.geef("bestaat_niet")


def test_betrekking_opsommen():
    """Als we een betrekking hebben met een zaak erin, krijgen we die terug."""
    zond = Zond()
    zaak = zond.zaak()

    class EenBetrekking(Betrekking):
        een_zaak = Zaak
        een_getal = int

    zond.betrek(EenBetrekking, een_zaak=zaak, een_getal=37)

    opsomming = list(zond.opsomming(EenBetrekking))
    assert len(opsomming) == 1
    een_betrekking = opsomming[0]
    assert een_betrekking.een_zaak == zaak
    assert een_betrekking.een_getal == 37


def test_betrekking_weghalen():
    """Als we een betrekking weer weghalen, krijgen we die niet te zien in de opsomming."""
    zond = Zond()
    zaak = zond.zaak()

    class EenBetrekking(Betrekking):
        een_zaak = Zaak
        een_getal = int

    betrekking = zond.betrek(EenBetrekking, een_zaak=zaak, een_getal=37)
    opsomming = list(zond.opsomming(EenBetrekking))
    assert len(opsomming) == 1

    zond.onttrek(betrekking)
    assert not list(zond.opsomming(EenBetrekking))


def test_tweeplaatsige_join():
    """Een tweeplaatsige betrekking geeft ons de mogelijkheid om te joinen."""
    zond = Zond()
    zaak1 = zond.zaak()
    zaak2 = zond.zaak()
    zaak3 = zond.zaak()

    class Eenplaatsig(Betrekking):
        de_zaak = Zaak

    class Tweeplaatsig(Betrekking):
        zaak_a = Zaak
        zaak_b = Zaak
        een_str = str

    zond.betrek(Eenplaatsig, de_zaak=zaak1)
    zond.betrek(Eenplaatsig, de_zaak=zaak2)
    zond.betrek(Tweeplaatsig, zaak_a=zaak1, zaak_b=zaak3, een_str="1 en 3")

    opsomming = list(zond.betrokken(Eenplaatsig.de_zaak, Tweeplaatsig.zaak_a))
    assert len(opsomming) == 1
    een, twee = opsomming[0]
    assert een.de_zaak == zaak1
    assert twee.zaak_a == zaak1
    assert twee.zaak_b == zaak3
    assert twee.een_str == "1 en 3"


def test_tweeplaatsig_zoeken():
    """Een tweeplaatsige betrekking geeft ons de mogelijkheid om te zoeken."""
    zond = Zond()
    zaak1 = zond.zaak()
    zaak2 = zond.zaak()
    zaak3 = zond.zaak()

    class Eenplaatsig(Betrekking):
        de_zaak = Zaak

    class Tweeplaatsig(Betrekking):
        zaak_a = Zaak
        zaak_b = Zaak
        een_str = str

    zond.betrek(Eenplaatsig, de_zaak=zaak1)
    zond.betrek(Eenplaatsig, de_zaak=zaak2)
    zond.betrek(Tweeplaatsig, zaak_a=zaak1, zaak_b=zaak3, een_str="1 en 3")

    opsomming = list(zond.opsomming(Eenplaatsig))
    betrekking1, betrekking2 = opsomming
    assert len(list(zond.zoek(Tweeplaatsig.zaak_a, betrekking1.de_zaak))) == 1
    assert len(list(zond.zoek(Tweeplaatsig.zaak_a, betrekking2.de_zaak))) == 0


def test_zondverwarring():
    """Als we twee verschillende Zond-objecten hebben, kun je niet een Zaak van de ene voeren aan de ander."""
    zond1 = Zond()
    zond2 = Zond()

    zaak1 = zond1.zaak()

    class EenBetrekking(Betrekking):
        de_zaak = Zaak

    with pytest.raises(ZondVerwarringError):
        zond2.betrek(EenBetrekking, de_zaak=zaak1)


def test_uniek_geven():
    """Als er maar een betrekking is die voldoet aan de eisen, geeft Zond die."""
    zond = Zond()
    zaak1 = zond.zaak()
    zaak2 = zond.zaak()
    zaak3 = zond.zaak()

    class Tweeplaatsig(Betrekking):
        zaak_a = Zaak
        zaak_b = Zaak
        een_str = str

    zond.betrek(Tweeplaatsig, zaak_a=zaak1, zaak_b=zaak3, een_str="1 en 3")
    zond.betrek(Tweeplaatsig, zaak_a=zaak2, zaak_b=zaak3, een_str="2 en 3")

    assert zond.geef(Tweeplaatsig.zaak_a, zaak2).een_str == "2 en 3"


def test_niet_uniek_geven():
    """Als er meerdere betrekkingen zijn die voldoen, gooit Zond een error."""
    zond = Zond()
    zaak1 = zond.zaak()
    zaak2 = zond.zaak()
    zaak3 = zond.zaak()

    class Tweeplaatsig(Betrekking):
        zaak_a = Zaak
        zaak_b = Zaak
        een_str = str

    zond.betrek(Tweeplaatsig, zaak_a=zaak1, zaak_b=zaak3, een_str="1 en 3")
    zond.betrek(Tweeplaatsig, zaak_a=zaak2, zaak_b=zaak3, een_str="2 en 3")

    with pytest.raises(NietUniekError):
        zond.geef(Tweeplaatsig.zaak_b, zaak3)


def test_reeds_betrokken():
    """Als we een betrekking al betrokken hebben, krijgen we een error."""
    zond = Zond()
    zaak1 = zond.zaak()
    zaak2 = zond.zaak()
    zaak3 = zond.zaak()

    class Betrekking1(Betrekking):
        zaak_a = Zaak
        zaak_b = Zaak
        een_str = str

    class Betrekking2(Betrekking):
        zaak_a = Zaak
        zaak_b = Zaak
        een_str = str

    zond.betrek(Betrekking1, zaak_a=zaak1, zaak_b=zaak3, een_str="1 en 2")
    zond.betrek(Betrekking1, zaak_a=zaak2, zaak_b=zaak3, een_str="2 en 3")
    zond.betrek(Betrekking2, zaak_a=zaak1, zaak_b=zaak3, een_str="1 en 3")
    zond.betrek(Betrekking2, zaak_a=zaak2, zaak_b=zaak3, een_str="2 en 3")

    uitkomst = zond.alles(Betrekking1).bind(Betrekking1.zaak_a, Betrekking2.zaak_a)

    with pytest.raises(ReedsBetrokkenError):
        uitkomst.bind(Betrekking2.zaak_b, Betrekking1.zaak_b).geef()


def test_beperken():
    """Als drie betrekkingen zijn gebonden, kunnen we beperken op twee ervan."""
    zond = Zond()
    zaak1 = zond.zaak()
    zaak2 = zond.zaak()
    zaak3 = zond.zaak()
    zaak4 = zond.zaak()

    class Betrekking1(Betrekking):
        zaak_a = Zaak
        zaak_b = Zaak
        een_str = str

    class Betrekking2(Betrekking):
        zaak_a = Zaak
        zaak_b = Zaak
        een_str = str

    class Betrekking3(Betrekking):
        zaak_a = Zaak
        zaak_b = Zaak
        een_str = str

    zond.betrek(Betrekking1, zaak_a=zaak1, zaak_b=zaak2, een_str="1 en 2")
    zond.betrek(Betrekking2, zaak_a=zaak2, zaak_b=zaak3, een_str="2 en 3")
    zond.betrek(Betrekking3, zaak_a=zaak3, zaak_b=zaak4, een_str="3 en 4")

    betrekkingen = (
        zond.alles(Betrekking1)
        .bind(Betrekking1.zaak_b, Betrekking2.zaak_a)
        .bind(Betrekking2.zaak_b, Betrekking3.zaak_a)
        .beperk(Betrekking2, Betrekking3)
        .geef()
    )
    assert betrekkingen[0].een_str == "2 en 3" and betrekkingen[1].een_str == "3 en 4"


def test_waarden():
    """Bij een uitkomst kunnen we de waarden van een kolom verkrijgen."""
    zond = Zond()
    zaak1 = zond.zaak()
    zaak2 = zond.zaak()
    zaak3 = zond.zaak()
    zaak4 = zond.zaak()

    class EenBetrekking(Betrekking):
        zaak_a = Zaak
        zaak_b = Zaak
        een_str = str

    zond.betrek(EenBetrekking, zaak_a=zaak1, zaak_b=zaak2, een_str="1 en 2")
    zond.betrek(EenBetrekking, zaak_a=zaak2, zaak_b=zaak3, een_str="2 en 3")
    zond.betrek(EenBetrekking, zaak_a=zaak3, zaak_b=zaak4, een_str="3 en 4")

    waarden = zond.alles(EenBetrekking).waarden(EenBetrekking.een_str)
    assert set(waarden) == {"1 en 2", "2 en 3", "3 en 4"}
